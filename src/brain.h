#ifndef __BRAIN_H__
#define __BRAIN_H__

#include <openssl/pem.h>
#include <sgx_urts.h>
#include <memory>
#include <string>
#include <vector>

#include <boost/asio.hpp>

#include "config.h"
#include "enclave_u.h"
#include "log.h"
#include "message.pb.h"
#include "user_type.h"
#include "util.h"

namespace p2p_sgx {

class Node;
// manage the enclave
class Brain {
 public:
  static const std::string kEnclaveFile;  // see value in brain.cc

  Brain(const Brain&) = delete;
  Brain& operator=(const Brain&) = delete;

  explicit Brain(Node& node, Config& config);
  ~Brain();

  void PeerHandler(PeerMessage& in, PeerMessage& out);
  void Unicast(FullAddress dst, const char* channel);

  void Start();
  void Update();

  sgx_enclave_id_t eid() { return eid_; }
  std::string StatusMessage(sgx_status_t status);

  void sleep_randomly();

 private:
  /* SGX enclave related */
  sgx_enclave_id_t eid_{0};
  sgx_launch_token_t token_{0};
  int updated_;
  sgx_status_t status_{SGX_ERROR_UNEXPECTED};  // the latest status

  /* used by EcallPeerhandler */
  char channel_[CHANNEL_MIN_SIZE];

  Node& node_;
  std::string host_addr_;
  std::size_t n_;
};

FullAddress full_address_from_string(std::string);
}

#endif /*  __BRAIN_H__ */
