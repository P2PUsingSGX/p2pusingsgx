#ifndef __ENCLAVE_H__
#define __ENCLAVE_H__

#include <openssl/dh.h>
#include <rapidjson/document.h>
#include <sgx_thread.h>
#include <memory>
#include <string>

#include "log.h"
#include "user_type.h"

namespace p2p_sgx {
namespace enclave {

class Mutex {
 public:
  Mutex() {
    // FIXME: static initialized sgx_thread_mutex is recommonded by Developer
    // Reference 1.6. However it needs every mutex_ become a global variable, //
    // since the macro will be considered as extended initializer list (a
    // feature not available in C++03) when using it in the initialization of
    // any non-static non-global variables.
    //
    // mutex_ = SGX_THREAD_MUTEX_INITIALIZER;

    /*
     * It's ok to explicitly call this function, since it is guranteed to not
     * have any race condition
     */
    sgx_thread_mutex_init(&mutex_, 0);
  }
  void Acquire() { sgx_thread_mutex_lock(&mutex_); }
  void Release() { sgx_thread_mutex_unlock(&mutex_); }

 private:
  sgx_thread_mutex_t mutex_;
};

class LockGuard {
 public:
  LockGuard(Mutex &mutex) : _ref(mutex) { _ref.Acquire(); }
  ~LockGuard() { _ref.Release(); }

 private:
  LockGuard(const LockGuard &);
  Mutex &_ref;
};

template <class T>
inline void hash_combine(size_t &seed, const T &v) {
  stlpmtx_std::hash<T> hasher;
  seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

struct Hash {
  std::size_t operator()(FullAddress fa) const {
    std::size_t seed = 0;
    hash_combine(seed, fa.ip);
    hash_combine(seed, fa.port);
    return seed;
  }
  std::size_t operator()(uint64_t id) const {
    std::size_t seed = 1;

    // FIXME: a workaround for solving the following issue.
    hash_combine(seed, (std::size_t)(id >> 32));
    hash_combine(seed, (std::size_t)(id & 0xffffffff));

    /*
     * using this single line will produce an error:
     * error: no match for call to
     * '(stlpmtx_std::hash<long long unsigned int>) (const long long unsigned
     * int&)'
     */
    // hash_combine(seed, id);

    return seed;
  }
};

/*
 * Utility
 */
FullAddress &id_to_fa(uint64_t &id);
uint64_t fa_to_id(FullAddress &fa);
void print_json(rapidjson::Document &d);
std::string base64_sha256(char *str);
std::auto_ptr<char> NewAutoPtrFromJson(rapidjson::Document &d);
std::string GetString(rapidjson::Document &d, const char *key);
} /* end of namespace enclave */
} /* end of namespace p2p_sgx */

#endif /* __ENCLAVE_H__ */
