#ifndef __ENCLAVE_CORE_H__
#define __ENCLAVE_CORE_H__

#include <openssl/bn.h>
#include <openssl/des.h>
#include <openssl/dh.h>
#include <sgx_tseal.h>

#include <stdio.h>
#include <cstdio>
#include <memory>
#include <unordered_map>
#include <unordered_set>

#include <rapidjson/document.h>
#include <sgx_tae_service.h>

#include "enclave.h"
#include "erb.h"
#include "erng.h"
#include "user_type.h"

namespace p2p_sgx {
namespace enclave {

class PeerProfile {
 public:
  static const std::size_t kAESKeyLen;
  static const std::size_t kAESIvLen;

  PeerProfile(const FullAddress &addr) {
    address_.port = addr.port;
    address_.ip = addr.ip;
    address_.align[0] = 0;
    address_.align[1] = 0;

    seq_ = *reinterpret_cast<uint64_t *>(&address_);

    std::fill(key_, key_ + kAESKeyLen, 0);

    // set a preconfigured key to measure the overhead
    for (int i = 0; i < 32; i++) key_[i] = 'A' + i;
    handshaked_ = true;
    // log_debug("PeerProfile Ctor seq = %llu", seq_);
  }

  bool ComputeShared(std::string pub_key, DH *core_dh);

  bool Encrypt(unsigned char *plain_text, std::size_t plain_len,
               unsigned char *cipher_text, std::size_t &cipher_len);
  bool Decrypt(unsigned char *cipher_text, std::size_t cipher_len,
               unsigned char *plain_text, std::size_t &plain_len);

  bool SetSecret(rapidjson::Document &d, rapidjson::Document &sd);
  bool SetSecret(rapidjson::Document &d, std::string &secret);
  bool GetSecret(rapidjson::Document &d, rapidjson::Document &sd);

  bool VerifyMac();
  bool ComputeMac();
  bool is_handshaked() { return handshaked_; }

  uint64_t GetSeq();
  uint64_t IncSeq();
  void SetSeq(uint64_t seq);

  FullAddress address_;
  Mutex mutex_;

 private:
  /*
   * AES 256 CBC mode is used.
   * key is 256-bit long.
   * iv is 128-bit long and shared by all peers
   */
  static const unsigned char iv_[16];  // XXX: hard-coded iv is not recommended
  unsigned char key_[32];

  uint64_t seq_;

  unsigned char *dh_shared_key_;
  BIGNUM *dh_pub_key_;

  bool handshaked_;
};

class Core {
 public:
  friend class ERNG;
  typedef std::tr1::unordered_map<FullAddress, PeerProfile, Hash>
      PeerProfileMap;
  typedef std::tr1::unordered_map<uint64_t, SRBP, Hash> SrbpMap;

  Core(FullAddress host_fa, FullAddress *peers, std::size_t count,
       InternalConfig iconf);
  ~Core();

  /*
   * Handshaking
   */
  bool IsHandshakeEnabled();
  void HandshakeHandler(PeerProfile &peer, rapidjson::Document &d);

  /*
   * Message
   */
  int MessageHandler(FullAddress source, rapidjson::Document &d);
  void Update();
  void Multicast(std::string &channel);
  void MulticastAll(std::string &channel);
  void Unicast(FullAddress receiver, char *channel);

  /*
   * PeerProfile
   */
  bool IsInitialPeer(FullAddress fa);
  PeerProfile &GetPeerProfile(FullAddress fa);
  uint64_t GetPeerSeq(uint64_t id);
  void IncPeerSeq(uint64_t id);

  /*
   * Experiments
   */
  // void StartSrbpDummy();
  void StartErbTest();
  void FinishErbTest();

  void StartErng();
  void FinishErng();

  /*
   * Others
   */
  uint64_t GetUptime();       // return the seconds passed since core's start
  uint64_t GetCurrentTime();  // return current_time_
  std::size_t TotalNodes();
  uint64_t HostId() { return host_id_; }

  InternalConfig in_config_;

 private:
  Core(const Core &);
  Core &operator=(const Core &);

  uint64_t precompute_erb_test_result(int erb_test_n);
  bool in_erb_test_mode();
  bool erb_test_finished_;

  /*
   * Core status
   */
  uint64_t host_id_;
  FullAddress host_fa_;
  DH *dh_;                  // core's DH setting
  bool handshake_enabled_;  // enable handshake or not
  SrbpMap srbp_map_;        // SRBP instances indexed by ID

  ERNG erng_;
  FullAddress *s_chosen_receiver_;
  std::size_t s_chosen_size_;

  /*
   * Gloabl status shared by different SRBP instances
   */
  // PeerProfile self_profile_;      // profile of itself
  PeerProfileMap initial_peers_;     // profiles of peers (including itself)
  FullAddress *fa_list_;             // peers FullAddress (including iteself)
  FullAddress *multicast_receiver_;  // other peers receving message (excluding)

  /*
   * SGX Time
   */
  sgx_time_t start_time_;
  sgx_time_t current_time_;
  sgx_time_source_nonce_t ref_nounce_;

  // Core's mutex
  Mutex mutex_;
};

} /* end of namespace enclave */
} /* end of namespace p2p_sgx */

#endif /*  __ENCLAVE_CORE_H__ */
