# P2PusingSGX-Code

Implementation for P2PUsingSGX

## Prerequisite

Compilation-required libraries:

* [Linux SGX SDK 1.6](https://github.com/01org/linux-sgx) (PSW is not required)
* [Boost 1.62.0](http://www.boost.org/)
* [Google Protobuf 3.1.0](https://github.com/google/protobuf)

Header-only libraries (already saved in `/vendor`):

* [Rapidjson](https://github.com/miloyip/rapidjson)
* [Spdlog](https://github.com/gabime/spdlog)

## Build

```bash
cd P2PUsingSGX-Code
make
```

## Experiment

The `experiment` directory contains scripts to launch multiple nodes under
different configurations. To be started, the compiled binary `p2psgx` and
dynamic library `enclave.signed.so` are needed in the directory, which is
already done by the root `Makefile`.

Three main scripts:

* `erb.sh`: multiple-node ERB-only experiment (without byzantine)
* `erb_byzantine.sh`: multiple-node ERB-only experiment (with byzantine)
* `erng.sh`: multiple-node ERNG experiment

After running any of these scripts, a new file `current_log_path` will indicate
the path of the logs of current run. All the logs will be located there.

## Disclaimer

This project is still under development.

## Reference

Updated later.




