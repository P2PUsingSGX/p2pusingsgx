#ifndef __LOG_H__
#define __LOG_H__

#include <memory>

#include "spdlog/spdlog.h"

namespace p2p_sgx {
extern std::shared_ptr<spdlog::logger> log;
extern std::shared_ptr<spdlog::logger> conn_log;
extern std::shared_ptr<spdlog::logger> brain_log;

#ifdef __cplusplus
extern "C" {
#endif
void enclave_logger(int level, const char *log_s);
#ifdef __cplusplus
}
#endif
}

#endif /* __LOG_H__ */
