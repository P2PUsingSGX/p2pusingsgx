#include <sgx_trts.h>
#include <stdint.h>
#include <cmath>

#include "core.h"
#include "erng.h"
#include "enclave_t.h"

namespace p2p_sgx {
namespace enclave {

ERNG::ERNG(Core& core, uint64_t id, uint64_t r1_max, uint64_t r2_max,
           uint64_t gamma)
    : state_(kUninitialized),
      core_(core),
      id_(id),
      gamma_(gamma),
      r1_max_(r1_max),
      r2_max_(r2_max) {}

ERNG::~ERNG() {}

void ERNG::Start(uint64_t start_t) {
  using namespace rapidjson;
  reset();

  init_time_ = start_t;

  /*
   * r1_max_ should be (n_ / 2 * gamma_ - 1), but currently we define it
   */
  r1_ = get_rand(r1_max_);
  log_debug("r1 = %llu, r1_max = %llu", r1_, r1_max_);

  if (r1_ == 0) {
    log_info("ERNG starts");
    s_chosen_.insert(id_);
    /*
     * setup next_multicast_ . it will be sent when TimeHandler is called
     */
    multicast_defer("CHOSEN", id_, core_.GetPeerSeq(id_), "", rnd_);
  }
}

void ERNG::reset() {
  state_ = kRunning;
  s_m_.clear();
  s_final_.clear();
  s_chosen_.clear();
  rnd_ = 1;
  m_.clear();
  next_msg_str_ = "";
}

void ERNG::TimerHandler() {
  if (state_ != kRunning) {
    return;  // stop TimerHandler
  }

  uint64_t elapsed = core_.GetCurrentTime() - init_time_;

  if (rnd_ > gamma_ + 4) {  // will be called only once
    // increment all peers' seq number
    log_info("ERNG ends, gamma = %llu", gamma_);
    for (Core::PeerProfileMap::iterator it = core_.initial_peers_.begin();
         it != core_.initial_peers_.end(); it++) {
      it->second.IncSeq();
    }

    state_ = kUninitialized;  // make it initial state
    return;
  }
  if (r1_ == 0 && rnd_ >= 3 && rnd_ <= gamma_ + 2) {
    // call the time handler of each ERB
    for (Core::SrbpMap::iterator it = core_.srbp_map_.begin();
         it != core_.srbp_map_.end(); it++) {
      it->second.TimerHandler();
    }
  }

  if (elapsed % core_.in_config_.erb_round_time ==
      core_.in_config_.erb_round_delay_time) {
    multicast_release();
  }

  // enter next round
  uint64_t new_round = 1 + elapsed / core_.in_config_.erb_round_time;
  if (new_round > rnd_) {
    log_info("================== ERNG Round %llu Start ==========", new_round);
    rnd_ = new_round;

    /* everytime new round is updated */
    if (r1_ == 0 && rnd_ == 2) {
      r2_ = get_rand(r2_max_);  // same situation with r1_max_
      log_debug("r2 = %llu, r2_max = %llu", r2_, r2_max_);
      // create ERB ..... with s_chosen
      log_info("================= ERNG deploy %llu ERB instances ===========",
               s_chosen_.size());
      for (IdSet::iterator i = s_chosen_.begin(); i != s_chosen_.end(); i++) {
        uint64_t erb_init_id = *i;
        std::pair<Core::SrbpMap::iterator, bool> result =
            core_.srbp_map_.insert(
                std::make_pair(erb_init_id, SRBP(core_, erb_init_id)));
        result.first->second.SetShortInitId(id_to_fa(erb_init_id).port);
        if (result.second) {
          log_debug("insertion succeeded, ERB.init_id = %llu",
                    result.first->second.init_id_);
        } else {
          log_debug("insertion failed, ERB.init_id should be = %llu",
                    result.first->first);
        }
        seq_backup_.insert(std::make_pair(*i, core_.GetPeerSeq(*i)));  // store
      }
    }

    if (r1_ == 0 && rnd_ == 3) {
      log_info("srbp_map.size() = %d", core_.srbp_map_.size());
      for (Core::SrbpMap::iterator it = core_.srbp_map_.begin();
           it != core_.srbp_map_.end(); it++) {
        log_debug("ERB[%llu] starts", it->first);
        if (r2_ == 0 && it->first == id_) {
          log_info("================= ERNG initials ERB[%llu] ===============",
                   id_);
          uint64_t rand_m = get_rand(UINT64_MAX);
          std::string m = get_base64_uint64(rand_m);
          log_info("rand_m = %llu, base64 = %s", rand_m, m.c_str());
          it->second.InitiatorStart(core_.GetCurrentTime(), m);
        } else {
          log_info("================= ERNG boots ERB[%llu] ===============",
                   id_);
          it->second.NonInitiatorStart(core_.GetCurrentTime());
        }
      }
    }

    if (r1_ == 0 && rnd_ == gamma_ + 3) {
      for (Core::SrbpMap::iterator it = core_.srbp_map_.begin();
           it != core_.srbp_map_.end(); it++) {
        if (it->second.IsDelivered()) {
          log_info("ERB[%llu] deliver %s", it->first,
                   it->second.GetM().c_str());
          m_.insert(it->second.GetM());
        } else {
          log_error("ERNG wait message failed: ERB[%llu].m empty", it->first);
        }
        uint64_t id = it->first;
        core_.GetPeerProfile(id_to_fa(id)).SetSeq(seq_backup_[id]);  // restore
      }
    }

    if (rnd_ == gamma_ + 4) {
      if (r1_ == 0) {
        s_m_.insert(m_);
        multicast_defer_final();
      }
    }
  }
}

std::string ERNG::get_base64_uint64(uint64_t rand_message) {
  std::auto_ptr<char> rand_buf(new char[sizeof(uint64_t)]);
  *reinterpret_cast<uint64_t*>(rand_buf.get()) = rand_message;

  std::auto_ptr<char> m_buf(new char[3 * sizeof(uint64_t)]);
  std::size_t len = util::b64_op(rand_buf.get(), sizeof(uint64_t), m_buf.get(),
                                 sizeof(uint64_t) * 3, util::kBase64Encode);
  *(m_buf.get() + len) = 0;

  std::string init_m(m_buf.get());

  return init_m;
}

int ERNG::Handler(FullAddress source, ErngMsg& msg) {
  if (rnd_ == 1) {
    if (msg.type() == "CHOSEN" && check_chosen_msg(source, msg)) {
      log_info(source, "--> CHOSEN");
      s_chosen_.insert(msg.id());
    }
  }

  if (r1_ == 0 && rnd_ == 2) {
    r2_ = get_rand(r1_ - 1);
    if (r2_ == 0) {
      // s
    }
  }

  if (rnd_ == gamma_ + 4) {
    if (msg.type() == "FINAL" && check_final_msg(source, msg)) {
      log_info(source, "--> FINAL");
      s_m_.insert(msg.set_m());
      for (std::set<MSet>::iterator it = s_m_.begin(); it != s_m_.end(); it++) {
        if (it->size() >= gamma_ + 1) {  // found any set
          s_final_ = *it;
          accept();
          break;
        }
      }
    }
  }

  return EPH_SUCCESS;
}

void ERNG::multicast_defer(std::string type, uint64_t id, uint64_t seq,
                           const char* m, uint64_t round) {
  using namespace rapidjson;
  Document d;
  d.SetObject();
  Document::AllocatorType& da = d.GetAllocator();

  d.AddMember("type", Value(type.c_str(), da).Move(), da);
  d.AddMember("id", id, da);
  d.AddMember("seq", seq, da);
  d.AddMember("m", Value(m, da).Move(), da);
  d.AddMember("round", round, da);

  StringBuffer d_buf;
  Writer<StringBuffer> writer(d_buf);
  d.Accept(writer);

  next_msg_str_ = d_buf.GetString();
}

void ERNG::multicast_defer_final() {
  using namespace rapidjson;
  Document d;
  d.SetObject();
  Document::AllocatorType& da = d.GetAllocator();

  Value m(kArrayType);
  for (MSet::iterator it = m_.begin(); it != m_.end(); it++) {
    if (!it->empty()) {
      m.PushBack(Value(it->c_str(), da).Move(), da);
    }
  }

  d.AddMember("type", "FINAL", da);
  d.AddMember("id", id_, da);
  d.AddMember("seq", core_.GetPeerSeq(id_), da);
  d.AddMember("m", m, da);
  d.AddMember("round", rnd_, da);

  StringBuffer d_buf;
  Writer<StringBuffer> writer(d_buf);
  d.Accept(writer);

  next_msg_str_ = d_buf.GetString();
}

void ERNG::halt() {
  if (state_ != kHalt) {
    log_critical("ERNG HALT");
  }
  state_ = kHalt;
}

void ERNG::multicast_release() {
  log_info("============= ERNG Round %llu Release ============", rnd_);
  if (next_msg_str_ == "") {
    // log_warn("<^> ERNG: No message to release");
    return;
  }
  log_info("<^> ERNG: %s", next_msg_str_.c_str());

  core_.MulticastAll(next_msg_str_);
  // clear multicast statuss
  next_msg_str_ = "";
}

/*
 * get_rand will combine the random number generated by untrusted and trusted
 * code.
 *
 * In SGX simulator, sgx_read_rand will generate identical pseudo-random number
 * sequence if programs are run at the same time.
 */
uint64_t ERNG::get_rand(uint64_t upper_bound) {
  uint64_t inside_rand, outside_rand;
  sgx_status_t ret = OcallGetRandUint64(&outside_rand);
  if (ret != SGX_SUCCESS) {
    log_error("#%s error: unable to get random number from unsafe outside",
              __func__);
    return UINT64_MAX;
  }
  sgx_read_rand(reinterpret_cast<unsigned char*>(&inside_rand),
                sizeof(inside_rand));
  if (ret != SGX_SUCCESS) {
    log_error("#%s error: unable to get random number from sgx", __func__);
    return UINT64_MAX;
  }

  double ratio = (double)(inside_rand ^ outside_rand) / UINT64_MAX;
  return upper_bound * ratio;
}

bool ERNG::check_chosen_msg(FullAddress& source, ErngMsg& msg) {
  if (msg.round() != 1) {
    log_error(source, "--> CHOSEN msg.round != 1");
    return false;
  }

  if (msg.seq() != core_.GetPeerSeq(msg.id())) {
    log_error(source, "--> CHOSEN msg.seq(%llu) != seq(%llu)", msg.seq(),
              core_.GetPeerSeq(msg.id()));
    return false;
  }

  return true;
}

bool ERNG::check_final_msg(FullAddress& source, ErngMsg& msg) {
  if (msg.round() != gamma_ + 4) {
    log_error(source, "--> FINAL msg.round(%llu) != gamma + 4 (%llu)",
              msg.round(), gamma_ + 4);
    return false;
  }

  if (msg.seq() != core_.GetPeerSeq(msg.id())) {
    log_error(source, "--> FINAL msg.seq(%llu) != seq(%llu)", msg.seq(),
              core_.GetPeerSeq(msg.id()));
    return false;
  }
  return true;
}

void ERNG::accept() {
  if (state_ == kAccepted) {
    return;
  }
  rand_r_ = 0;
  for (MSet::iterator it = s_final_.begin(); it != s_final_.end(); it++) {
    std::string m = *it;
    uint64_t it_rand;

    std::auto_ptr<char> m_buf(new char[m.size()]);
    m.copy(m_buf.get(), m.size());
    util::b64_op(m_buf.get(), m.size(), reinterpret_cast<char*>(&it_rand),
                 sizeof(it_rand), util::kBase64Decode);

    rand_r_ ^= it_rand;
  }
  state_ = kAccepted;
}

bool ERNG::IsAccepted() { return state_ == kAccepted; }

uint64_t ERNG::GetReliableRand() {
  return rand_r_;
}



} /* end of namespace enclave */
} /* end of namespace p2p_sgx */
