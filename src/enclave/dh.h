#ifndef __ENCLAVE_CRYPTO__
#define __ENCLAVE_CRYPTO__

#include <openssl/dh.h>

namespace p2p_sgx {

namespace enclave {

DH *get_dh2236();

}
}

#endif /* __ENCLAVE_CRYPTO__ */
