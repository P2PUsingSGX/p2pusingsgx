/*
 * brain.cc
 *
 * all enclave related functions
 */

#include <openssl/pem.h>
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>
#include <cstdio>
#include <map>
#include <memory>
#include <random>
#include <thread>

#include "brain.h"
#include "cmd.h"
#include "enclave_u.h"
#include "log.h"
#include "message.pb.h"
#include "node.h"
#include "user_type.h"
#include "util.h"

namespace p2p_sgx {

const std::string Brain::kEnclaveFile = "enclave.signed.so";

Brain::Brain(Node& node, Config& config)
    : node_(node), host_addr_(config.host_ip + ":" + config.host_port) {
  brain_log->debug("random seed {}", config.host_port);
  std::srand(atoi(config.host_port.c_str()));

  if (SGX_DEBUG_FLAG) {
    brain_log->info("Enclave debug mode enabled");
  }
  status_ = sgx_create_enclave(kEnclaveFile.c_str(), SGX_DEBUG_FLAG, &token_,
                               &updated_, &eid_, nullptr);
  if (SGX_SUCCESS != status_) {
    brain_log->error("SGX error: {}", StatusMessage(status_));
    return;
  }

  std::unique_ptr<FullAddress[]> ptr(new FullAddress[config.peers.size()]);

  auto i = 0;
  for (auto& peer : config.peers) {
    ptr[i++] = full_address_from_string(peer);
  }
  n_ = config.peers.size();
  status_ = EcallInitializeCore(eid_, ptr.get(), config.peers.size(),
                                full_address_from_string(host_addr_),
                                config.ToInternalConfig());
  if (status_ != SGX_SUCCESS) {
    brain_log->error("EcallInitializeCore failed: {}", StatusMessage(status_));
  }
  brain_log->trace("brain successfully load {} (eid={})", kEnclaveFile, eid_);
}

Brain::~Brain() {
  auto ret = sgx_destroy_enclave(eid_);
  if (ret != SGX_SUCCESS) {
    brain_log->error("brain failed to destroy the enclave: {}",
                     StatusMessage(ret));
  } else {
    brain_log->trace("brain destroyed the enclave");
  }
}

FullAddress full_address_from_string(std::string addr) {
  auto ip_slice = addr.substr(0, addr.find_first_of(':'));
  auto port_slice = addr.substr(addr.find_last_of(':') + 1).c_str();

  // SPDLOG_DEBUG(brain_log, "full_address_from_string({}) -> ip: {} port: {}",
  // addr, ip_slice, port_slice);

  std::istringstream is(addr);

  uint8_t ip[4];
  uint16_t port;
  try {
    for (auto& i : ip) {
      int seg;
      is >> seg;
      i = seg & 0xff;
      is.ignore(1);
    }
    is >> port;
  } catch (std::exception& e) {
    brain_log->error("#{} exception: {}", __func__, e.what());
  }
  // SPDLOG_DEBUG(brain_log, "{} {} {} {}", ip[0], ip[1], ip[2], ip[3]);

  uint32_t ip_32 = (ip[3] << 24) + (ip[2] << 16) + (ip[1] << 8) + ip[0];

  FullAddress new_fa;
  new_fa.align[0] = 0;
  new_fa.align[1] = 0;
  new_fa.ip = ip_32;
  new_fa.port = port;
  return new_fa;
}

std::string full_address_to_string(FullAddress fa) {
  std::ostringstream os;
  struct IpSeg {
    uint8_t s1, s2, s3, s4;
  };

  IpSeg* is = reinterpret_cast<IpSeg*>(&fa.ip);
  os << (int)is->s1 << "." << (int)is->s2 << "." << (int)is->s3 << "."
     << (int)is->s4 << ":" << fa.port;
  return os.str();
}

// Start() will start the SRBP protocol
void Brain::Start() {
  auto ret = EcallStartCore(eid_);
  if (ret != SGX_SUCCESS) {
    brain_log->error("SetSbrpInit SGX error: {}", StatusMessage(ret));
    status_ = ret;
  }
}

void Brain::Update() {
  auto ret = EcallTimer(eid_);
  if (ret != SGX_SUCCESS) {
    brain_log->error("EcallTimer error: {}", StatusMessage(ret));
    status_ = ret;
  }
}

std::string GetJsonString(rapidjson::Document& d, const char* key) {
  std::string out = d.IsObject() && d.HasMember(key) && d[key].IsString()
                        ? d[key].GetString()
                        : "";
  return out;
}

// FIXME: it is not safe to use StringRef, see
// http://rapidjson.org/md_doc_tutorial.html#CreateString
bool MessageToJson(PeerMessage& m, rapidjson::Document& d) {
  using namespace rapidjson;

  SPDLOG_TRACE(brain_log, "#{} inspect protobuf: {}", __func__,
               m.DebugString());
  d.SetObject();  // clear the rapidjson object
  if (m.has_pub()) {
    auto pk = m.pub().public_key();
    d.AddMember("type", "pub", d.GetAllocator());
    d.AddMember("pub_key", Value(pk.c_str(), d.GetAllocator()),
                d.GetAllocator());
  } else if (m.has_data()) {
    auto sc = m.data().secret();

    d.AddMember("type", "data", d.GetAllocator());
    d.AddMember("secret", Value(sc.c_str(), d.GetAllocator()),
                d.GetAllocator());

    SPDLOG_TRACE(brain_log, "#{} secret text: {}", __func__, sc.c_str());
  } else if (m.has_ack()) {
    // do nothing
    return false;
  }
  return true;
}

bool JsonToMessage(rapidjson::Document& d, PeerMessage& m) {
  if (d.HasParseError()) {
    return false;
  }

  m.Clear();
  std::string type = GetJsonString(d, "type");
  if (type == "pub") {
    std::string pk = GetJsonString(d, "pub_key");
    auto pub = m.mutable_pub();
    pub->set_public_key(pk);
  } else if (type == "data") {
    std::string st = GetJsonString(d, "secret");
    auto data = m.mutable_data();
    data->set_secret(st);
  } else {
    m.mutable_ack();  // document is unreadable, return ack instead
  }
  return true;
}

void Brain::PeerHandler(PeerMessage& in, PeerMessage& out) {
  using namespace rapidjson;
  auto origin = in.origin();

  // let ack become default reply message
  out.Clear();
  out.mutable_ack();

  Document d;

  /* protobuf -> json */
  if (!MessageToJson(in, d)) {
    return;  // and not continue
  }

  /* json -> channel */
  StringBuffer d_buf;
  Writer<StringBuffer> writer(d_buf);
  d.Accept(writer);

  SPDLOG_TRACE(brain_log, "{} #{} channel input {}", origin, __func__,
               d_buf.GetString());
  std::size_t len = d_buf.GetSize() + 1;

  if (len > CHANNEL_MIN_SIZE) {
    brain_log->error("{} #{} channel input larger than {}", origin, __func__,
                     CHANNEL_MIN_SIZE);
    return;
  }

  // need to copy '\0'
  std::copy(d_buf.GetString(), d_buf.GetString() + len, channel_);

  /* EcallPeerHandler */
  int eph_ret;
  FullAddress src_fa = full_address_from_string(in.origin());
  int ch_cap = CHANNEL_MIN_SIZE;  // channel capacity
  auto ret = EcallPeerHandler(eid_, &eph_ret, src_fa, channel_, ch_cap);

  if (ret != SGX_SUCCESS || eph_ret != EPH_SUCCESS) {
    if (ret != SGX_SUCCESS) {
      brain_log->error("{} #{} Ecall error: {}", origin, __func__,
                       StatusMessage(ret));
    }
    return;
  }

  SPDLOG_TRACE(brain_log, "{} #{} channel output {}", origin, __func__,
               channel_);

  /* channel -> json */
  d.Parse(channel_);

  /* json -> protobuf */
  if (!JsonToMessage(d, out)) {
    return;
  }
}

/* put the channel message into PeerMessage "data" */
void Brain::Unicast(FullAddress dst, const char* channel) {
  PeerMessage msg;
  rapidjson::Document d;

  SPDLOG_TRACE(brain_log, "#{} channel = {}", __func__, channel);
  d.Parse(channel);

  /* json -> message */
  if (!JsonToMessage(d, msg)) {
    brain_log->error("Brain::Unicast JsonToMessage fails");
    return;
  }

  double rand = (double)std::rand() / RAND_MAX;
  std::size_t delay = p2p_sgx::Connection::kStartDelay * rand;
  SPDLOG_DEBUG(conn_log, "connection start delay {}", delay);
  // timer_.expires_from_now(boost::posix_time::milliseconds(delay));

  node_.Send(full_address_to_string(dst), std::move(msg), delay);
  SPDLOG_TRACE(brain_log, "{} #{}", full_address_to_string(dst), __func__);
}

std::string Brain::StatusMessage(sgx_status_t status) {
  static std::map<sgx_status_t, std::string> msgs{
      {
          SGX_ERROR_UNEXPECTED, "Unexpected error occurred.",
      },
      {
          SGX_ERROR_INVALID_PARAMETER, "Invalid parameter.",
      },
      {
          SGX_ERROR_OUT_OF_MEMORY, "Out of memory.",
      },
      {
          SGX_ERROR_ENCLAVE_LOST, "Power transition occurred.",
      },
      {
          SGX_ERROR_INVALID_ENCLAVE, "Invalid enclave image.",
      },
      {
          SGX_ERROR_INVALID_ENCLAVE_ID, "Invalid enclave identification.",
      },
      {
          SGX_ERROR_INVALID_SIGNATURE, "Invalid enclave signature.",
      },
      {
          SGX_ERROR_OUT_OF_EPC, "Out of EPC memory.",
      },
      {
          SGX_ERROR_NO_DEVICE, "Invalid SGX device.",
      },
      {
          SGX_ERROR_MEMORY_MAP_CONFLICT, "Memory map conflicted.",
      },
      {
          SGX_ERROR_INVALID_METADATA, "Invalid enclave metadata.",
      },
      {
          SGX_ERROR_DEVICE_BUSY, "SGX device was busy.",
      },
      {
          SGX_ERROR_INVALID_VERSION, "Enclave version was invalid.",
      },
      {
          SGX_ERROR_INVALID_ATTRIBUTE, "Enclave was not authorized.",
      },
      {
          SGX_ERROR_ENCLAVE_FILE_ACCESS, "Can't open enclave file.",
      },
  };

  if (msgs.count(status)) {
    return msgs[status];
  }
  return msgs[SGX_ERROR_UNEXPECTED];
}

void Brain::sleep_randomly() {
  double ratio = (double)std::rand() / RAND_MAX;
  uint64_t t = (double)(ratio * 0.5) *
               (global_node->config_.erb_round_time -
                global_node->config_.erb_round_delay_time) *
               1000 / n_;

  std::this_thread::sleep_for(std::chrono::milliseconds(t));
}

} /* end of namespace p2p_sgx */

void OCallUnicast(FullAddress dst, const char* channel) {
  SPDLOG_TRACE(p2p_sgx::brain_log, "OcallUnicast");
  global_node->brain_.Unicast(dst, channel);
}

void OcallMulticast(FullAddress dsts[], size_t count, const char* channel) {
  SPDLOG_TRACE(p2p_sgx::brain_log, "OcallMulticast");
  // global_node->brain_.sleep_randomly();
  for (auto i = 0; i < count; i++) {
    global_node->brain_.Unicast(dsts[i], channel);
  }
}

void OcallFinish() { global_node->PrintReport(); }

uint64_t OcallGetRandUint64() {
  static std::mt19937 g(std::random_device{}());
  std::uniform_int_distribution<uint64_t> uniform_dist(
      0, std::numeric_limits<uint64_t>::max());
  uint64_t ret = uniform_dist(g);
  // std::cout << "Randomly-chosen mean: " << ret << '\n';
  return ret;
}
