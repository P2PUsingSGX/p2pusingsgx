#include "log.h"

namespace p2p_sgx {

std::shared_ptr<spdlog::logger> log = spdlog::stdout_color_mt("conso");
std::shared_ptr<spdlog::logger> conn_log = spdlog::stdout_color_mt("conne");
std::shared_ptr<spdlog::logger> brain_log = spdlog::stdout_color_mt("brain");
std::shared_ptr<spdlog::logger> enclave_log = spdlog::stdout_color_mt("encla");

void enclave_logger(int level, const char *log_s) {
  std::string s(log_s);
  switch (level) {
    case -3:
     enclave_log->critical(s);
      break;
    case -2:
     enclave_log->trace(s);
      break;
    case -1:
     enclave_log->debug(s);
      break;
    case 1:
     enclave_log->warn(s);
      break;
    case 2:
     enclave_log->error(s);
      break;
    default:
     enclave_log->info(s);
      break;
  }
}

}
