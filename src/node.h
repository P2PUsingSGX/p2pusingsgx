#ifndef __NODE_H__
#define __NODE_H__

#include <atomic>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <boost/asio.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "brain.h"
#include "cmd.h"
#include "config.h"
#include "connection.h"
#include "log.h"

namespace p2p_sgx {

class Node {
 public:
  using ConnectionSet = std::unordered_set<ConnectionPtr>;
  using ConnectionMap = std::unordered_map<std::string, ConnectionPtr>;
  friend Connection;
  friend Brain;
  Node(const Node&) = delete;
  Node& operator=(const Node&) = delete;

  explicit Node(Config& config);

  void Run();  // when everything is ready, call run to block
  void Stop();

  // for listening connection
  void InsertConnection(std::shared_ptr<Connection>);
  void DeleteConnection(std::shared_ptr<Connection>);

  void Send(std::string addr, PeerMessage msg);
  void Send(std::string addr, PeerMessage msg, std::size_t ms);

  void PrintReport();

  Config config_;
  Brain brain_;

  // count the sending connection
  int listen_connection_count{0};
  int send_connection_count{0};

  std::size_t message_in_{0}, message_out_{0};
  std::size_t message_rx_{0}, message_tx_{0};
 private:
  class SendDelayer : public std::enable_shared_from_this<SendDelayer> {
   public:
    SendDelayer(const SendDelayer&) = delete;
    SendDelayer& operator=(const SendDelayer&) = delete;
    SendDelayer(Node& node, std::string addr, PeerMessage msg, std::size_t ms)
        : node_(node),
          addr_(std::move(addr)),
          msg_(std::move(msg)),
          t_(node.io_service_, boost::posix_time::milliseconds(ms)) {
      t_.async_wait(std::bind(&SendDelayer::send, this));
    }

   private:
    void send() {
      node_.Send(addr_, std::move(msg_));
      node_.timers_.erase(shared_from_this());
    }

    Node& node_;
    boost::asio::deadline_timer t_;
    std::string addr_;
    PeerMessage msg_;
  };
  void AsyncAccept();
  // void AsyncConnect(std::string addr, PeerMessage msg);
  void AsyncTimer();

  std::string next_byzantine_;
  bool is_byzantine_{false};

  // the base service of all boost asio functions
  boost::asio::io_service io_service_;

  boost::asio::signal_set signals_;
  boost::asio::ip::tcp::socket socket_;
  boost::asio::ip::tcp::acceptor acceptor_;
  boost::asio::deadline_timer timer_;

  std::unordered_set<std::shared_ptr<SendDelayer>> timers_;

  ConnectionSet connections_;    // for listening
  ConnectionMap sending_conns_;  // for sending
};
}

#endif /* __NODE_H__ */
