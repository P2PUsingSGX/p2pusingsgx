

if [ -z $1 ]
then
  log_path=$(cat current_log_path)
else
  log_path=$1
fi

echo ERB test success:
grep -r "ERB test success" $log_path | wc -l
echo

echo ERB delivered count per node
grep -r -c "Deliver" $log_path | head -n 5
echo

echo ERNG Unsuccessful
grep -r -L "ERNG" $log_path | head -5
echo

echo "Error: any error" $(grep -r -l "error" $log_path | wc -l)
grep -r -l "error" $log_path | head -5
echo

echo "Error: Async* error" $(grep -r -l "Async" $log_path | wc -l)
grep -r -l "Async" $log_path | head -5
echo

echo "Error: Connection reset by peer" $(grep -r -l "Connection reset by peer" $log_path | wc -l)
grep -r -l "Connection reset by peer" $log_path | head -5
echo

echo "Error: Too many open files" $(grep -r -l "Too many open files" $log_path | wc -l)
grep -r -l "Too many open files" $log_path | head -5
echo

echo "Error: Connection refused" $(grep -r -l "refused" $log_path | wc -l)
grep -r -l "refused" $log_path | head -5
