#include <openssl/buffer.h>
#include <algorithm>
#include <memory>

#include <memory>
#include "util.h"

namespace p2p_sgx {
namespace util {

char* NewBufferFromJson(const rapidjson::Document& d) {
  using namespace rapidjson;

  StringBuffer buffer;
  Writer<StringBuffer> writer(buffer);
  d.Accept(writer);

  const char* start = buffer.GetString();
  int len = buffer.GetSize() + 1;
  char* buf = new char[len];
  std::copy(start, start + len, buf);
  buf[len - 1] = 0;

  return buf;
}

int b64_op(char* in, int in_len, char* out, int out_len, int op) {
  int ret = 0;
  BIO* b64 = BIO_new(BIO_f_base64());
  BIO* bio = BIO_new(BIO_s_mem());
  BIO_push(b64, bio);

  BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
  if (op == 0) {
    ret = BIO_write(b64, in, in_len);
    BIO_flush(b64);
    if (ret > 0) {
      ret = BIO_read(bio, out, out_len);
    }

  } else {
    ret = BIO_write(bio, in, in_len);
    BIO_flush(bio);
    if (ret) {
      ret = BIO_read(b64, out, out_len);
    }
  }
  BIO_free(b64);
  return ret;
}

char* Base64Encode(const char* input, int length, bool with_new_line) {
  BIO* bmem = NULL;
  BIO* b64 = NULL;
  BUF_MEM* bptr = NULL;

  b64 = BIO_new(BIO_f_base64());
  if (!with_new_line) {
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
  }
  bmem = BIO_new(BIO_s_mem());
  b64 = BIO_push(b64, bmem);
  BIO_write(b64, input, length);
  BIO_flush(b64);
  BIO_get_mem_ptr(b64, &bptr);

  char* buff = (char*)malloc(bptr->length + 1);
  memcpy(buff, bptr->data, bptr->length);
  buff[bptr->length] = 0;

  BIO_free_all(b64);

  return buff;
}

char* Base64Decode(char* input, int length, bool with_new_line) {
  BIO* b64 = NULL;
  BIO* bmem = NULL;
  char* buffer = (char*)malloc(length);
  memset(buffer, 0, length);

  b64 = BIO_new(BIO_f_base64());
  if (!with_new_line) {
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
  }
  bmem = BIO_new_mem_buf(input, length);
  bmem = BIO_push(b64, bmem);
  BIO_read(bmem, buffer, length);

  BIO_free_all(bmem);

  return buffer;
}
}
}
