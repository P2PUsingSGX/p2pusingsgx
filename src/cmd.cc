#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <thread>
#include <vector>
#include <cstdlib>

#include <boost/asio.hpp>
#include <boost/program_options.hpp>

#include <openssl/bn.h>

#include "log.h"
#include "message.pb.h"
#include "node.h"
#include "util.h"
#include "cmd.h"

using namespace std;

/* global_node is assigned in main and will provide entry point for ocalls */
p2p_sgx::Node* global_node{nullptr};

int main(int argc, char** argv) {
  namespace po = boost::program_options;
  std::string arg0(argv[0]);
  std::string prog_name = arg0.substr(arg0.find_last_of('/') + 1);

  try {

    p2p_sgx::Config config;

    int debug_level;
    std::string test;
    std::string config_file;
    std::string address;


    po::options_description file_conf("Configuration File Options");

    file_conf.add_options()
      ("peer,e", po::value<vector<string>>(&config.peers)->composing(), "peer address (can be set for multiple times)");

    po::options_description cmd("Command Line Options");
    cmd.add_options()
      ("help,h", "print this help")
      ("level,l", po::value<int>(&debug_level)->default_value(0), "set log level (0:normal 1:debug 2:trace)")
      ("verbose,v", "log with date and time")
      ("config,c", po::value<string>(&config_file)->default_value(prog_name + ".conf"), "config file name")
      ("address,a", po::value<string>(&address)->default_value("127.0.0.1:5000"), "IP address and port")
      ("test,t", po::value<string>(&test)->default_value("i/n"), "set node #i (1 <= i <= n), will overwrite '--address'")
      ("round,r", po::value<int>(&config.erb_round_time)->default_value(2), "ERB round time")
      ("delay,d", po::value<int>(&config.erb_round_delay_time)->default_value(1), "ERB round delay time")
      ("nerb,n", po::value<int>(&config.erb_test_n)->default_value(1), "number of running ERB instances")
      ("byzantine,b", po::value<int>(&config.erb_test_byzantine_n)->default_value(0), "enable byzantine nodes and set maximum value (0: disable)")
      ("mode,m", po::value<int>(&config.erb_test_mode)->default_value(1), "ERB-only mode (1) or ERNG mode (0)")
      ;
    cmd.add(file_conf);

    po::options_description config_file_options("Config File");
    config_file_options.add_options()
      ("reconnect_wait_time", po::value<int>(&config.reconnect_wait_time)->default_value(1000))
      ("multicast_wait_ratio", po::value<double>(&config.multicast_wait_ratio)->default_value(0.5))
      ("erng_r1_max", po::value<int>(&config.erng_r1_max)->default_value(0))
      ("erng_r2_max", po::value<int>(&config.erng_r2_max)->default_value(0))
      ;
    config_file_options.add(file_conf);

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, cmd), vm);
    po::notify(vm);

    std::ifstream ifs{config_file};
    if (ifs) {
      store(parse_config_file(ifs, config_file_options), vm);
      notify(vm);
    }

    if (!vm.count("verbose")) {
      /*
       * %L - short log level of the message. "D" for "debug", "I" for "info".
       * %n - logger's name
       *
       * others: https://github.com/gabime/spdlog/wiki/3.-Custom-formatting
       */
      spdlog::set_pattern("[%L][%n] %v");
    }

    if (vm.count("help")) {
      cout << cmd << "\n";
      return EXIT_FAILURE;
    }

    switch (debug_level) {
      case 1:
        spdlog::set_level(spdlog::level::debug);
        break;
      case 2:
        spdlog::set_level(spdlog::level::trace);
        break;
      case 0:
      default:
        spdlog::set_level(spdlog::level::info);
    }
    /*
     * address -> config.host_ip and config.host_port
     */
    auto pos = address.find_first_of(':');
    config.host_ip = address.substr(0, pos);
    config.host_port = address.substr(pos + 1);
    /*
     * option "test"
     *
     *   This option setup a test node i in all n nodes.
     *
     *   This node will have
     *     ip:   127.0.0.1
     *     port: config.host_port + i
     *
     *   It also generate a preset of peers to be loaded in to config.peers
     */
    if (test != "i/n") { // "i/n" is the placeholder
      istringstream is(test);
      int i, n;
      is >> i;
      is.ignore(1);
      is >> n;
      vector<string> preset_peers;
      if (n <= 0 || i <= 0 || i > n) {
        cerr << cmd << "\n";
        return EXIT_FAILURE;
      }
      for (auto j = 1; j <= n; j++) {
        ostringstream os;
        os << "127.0.0.1:" << atoi(config.host_port.c_str()) + j;
        preset_peers.push_back(os.str());
      }
      ostringstream os;
      os << atoi(config.host_port.c_str()) + i;

      config.peers = preset_peers;
      config.host_port = os.str();
    }

    if (config.erb_round_time < 1 || config.erb_round_delay_time < 1) {
      cout << cmd << endl;
      return EXIT_FAILURE;
    }

    if (config.erb_test_n > config.peers.size() + 1) {
      cerr << "option '--erbn' should be a number between 1 and n\n";
      cout << cmd << endl;
      return EXIT_FAILURE;
    }

    if (config.erb_test_byzantine_n > 0) {
      config.erb_test_n = 1;
    }

    if (config.peers.size() < 2) {
      cerr << "too few peers for running (minimum is 2)\n";
      cout << cmd << endl;
      return EXIT_FAILURE;
    }

    global_node = new p2p_sgx::Node(config);
    global_node->Run();

  } catch (const po::error& e) {
    cerr << prog_name << ": " << e.what() << "\n";
    cerr << "try '" << prog_name << " --help' for more information.\n";
    return EXIT_FAILURE;
  } catch (std::exception& e) {
    cerr << "exited due to runtime exception: " << e.what() << "\n";
    return EXIT_FAILURE;
  }
}
