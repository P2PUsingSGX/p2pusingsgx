#!/bin/sh

project_root="~/workspace/P2PusingSGX-Code"

server_hosts="
cloud1
cloud2
"

# use get to pull ifconfig results from servers and put right IPs here
server_exp_ips=(
"172.26.186.71"
"172.26.187.140"
)

usage() {
    echo "Usage: $1 [subcommand]
    get	   	get ifconfig result for each host
    config N	config nodelist file and cmd.conf (N nodes per host)
    deploy 	deploy setup file for each host
    start R D	start multiple server run (R=round, D=delay)
    stop	stop multiple server run
    result 	retrieve the result and show"
}

for_run() {
    for i in $server_hosts
    do
        echo running on node "$i": $@
        ssh $i "$@"
    done
}

foreach_run() {
    for i in $server_hosts
    do
        echo running on node "$i": $@
        ssh $i "$@ &" >> dolog &
    done
}

config_nodelist() {
    if [ -z $1 ]
    then
	usage
	exit 1
    fi

    rm -f cmd.conf
    echo "reconnect_wait_time = 1000 # milliseconds" >> cmd.conf
    echo "multicast_wait_ratio = 0.5" >> cmd.conf

    n=$1
    host_i=0
    for host in $server_hosts
    do
	filename=$host.nodelist
	rm -f $filename
	ip=${server_exp_ips[$host_i]}
	for i in `seq $n`
	do
	    peer=$ip:$((5000 + $i))
	    echo "$peer" >> $filename
	    echo "peer = $peer" >> cmd.conf
	done
	host_i=$((host_i + 1))
	echo $filename generated
    done
    echo cmd.conf generated
}

deploy_nodelist() {
    for host in $server_hosts
    do
	scp ${host}.nodelist $host:$project_root/src
	scp cmd.conf $host:$project_root/src
    done
}

case $1 in
    get)
        echo "Getting server ifconfig results"
        for_run "ifconfig em1 | grep \"inet addr\""
	;;
    config)
	config_nodelist $2
	;;
    deploy)
	deploy_nodelist
	;;
    start)
	if [ -z $2 ] || [ -z $3 ]
	then
	    usage
	    exit 1
	fi
	echo `date +"%Y%m%d-%H%M%S"` $@ >> dolog
	foreach_run $project_root/src/mrun.sh $2 $3
	;;
    stop)
	echo `date +"%Y%m%d-%H%M%S"` $@ >> dolog
	foreach_run $project_root/src/kill.sh
	;;
    result)
	echo `date +"%Y%m%d-%H%M%S"` $@ >> dolog
	for_run $project_root/src/count.sh
	;;
    any)
	shift
	for_run $@
	;;
    *)
	usage $0
	;;
  esac

