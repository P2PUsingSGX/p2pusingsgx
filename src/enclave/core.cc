#include <openssl/bio.h>
#include <openssl/des.h>
#include <sgx_trts.h>

#include "core.h"
#include "dh.h"
#include "enclave.h"
#include "enclave_t.h"
#include "log.h"
#include "user_type.h"
#include "util.h"
#include "base64.h"

namespace p2p_sgx {
namespace enclave {

Core::Core(FullAddress host_fa, FullAddress *peers, std::size_t count,
           InternalConfig iconf)
    : dh_(get_dh2236()),
      handshake_enabled_(true),
      // self_profile_(host_fa),
      erb_test_finished_(false),
      host_fa_(host_fa),
      in_config_(iconf),
      host_id_(*reinterpret_cast<uint64_t *>(&host_fa)),
      erng_(*this, host_id_, iconf.erng_r1_max, iconf.erng_r2_max,
            count * ((double)1 / iconf.erng_r1_max)) {
  sgx_status_t ret;

  /*
   * setup start time
   */
  ret = sgx_create_pse_session();
  if (ret != SGX_SUCCESS) {
    log_error("Core() error: failed to create PSE session");
    return;
  }
  ret = sgx_get_trusted_time(&start_time_, &ref_nounce_);
  if (ret != SGX_SUCCESS) {
    log_error("#%s error: failed to get trusted time from sgx", __func__);
    return;
  }
  current_time_ = start_time_;

  /*
   * check DH parameters and generate keys
   */
  int retcodes;
  if (!DH_check(dh_, &retcodes)) {
    log_error("core has a insecure DH params");
    return;
  }
  if (!DH_generate_key(dh_)) {
    log_error("core failed to generate key from DH params");
    return;
  }
  log_trace("core's DH initialized");

  /*
   * construct profiles for other peers
   */
  fa_list_ = new FullAddress[count];
  multicast_receiver_ = new FullAddress[count - 1];
  std::size_t r_count = 0;
  for (std::size_t i = 0; i < count; i++) {
    fa_list_[i] = peers[i];
    if (!(peers[i] == host_fa_)) {
      multicast_receiver_[r_count++] = peers[i];
    }
    initial_peers_.insert(std::make_pair(peers[i], PeerProfile(peers[i])));
  }

  /*
   * Set key paramters for SRBP
   */
  SRBP::kRoundCycle = in_config_.erb_round_time;
  SRBP::kRoundSendDelay = in_config_.erb_round_delay_time;

  log_info("=========== Core configuration ==========");
  log_info(host_fa_, "=> id = %llu", host_id_);
  log_info("peers: %llu", initial_peers_.size());
  // for (int i = 0; i < initial_peers_.size(); i++) {
  // log_info(fa_list_[i], "=> id = %llu", fa_to_id(fa_list_[i]));
  // }
  log_info("start time: %llu", start_time_);
  log_info("RoundCycle %llu, RoundDelay %llu", SRBP::kRoundCycle,
           SRBP::kRoundSendDelay);
  log_info("=========================================");

  if (in_erb_test_mode()) {
    StartErbTest();
  } else {
    StartErng();
  }
}

Core::~Core() {
  sgx_close_pse_session();  // close regardless any error

  if (fa_list_) {
    delete[] fa_list_;
    fa_list_ = NULL;
  }
}

bool Core::IsInitialPeer(FullAddress fa) {
  return initial_peers_.find(fa) != initial_peers_.end();
}

PeerProfile &Core::GetPeerProfile(FullAddress fa) {
  // if (fa == host_fa_) {
  // return self_profile_;
  // }
  return initial_peers_.find(fa)->second;
}

bool Core::IsHandshakeEnabled() { return handshake_enabled_; }

void Core::Update() {
  // std::string dummy_message = "123456";
  // Multicast((char *)dummy_message.c_str());

  // return;

  /////////////////
  LockGuard lg(mutex_);  // lock

  if (SGX_SUCCESS != sgx_get_trusted_time(&current_time_, &ref_nounce_)) {
    log_error("#%s error: unable to get trusted time", __func__);
    current_time_ = start_time_;
    return;
  }

  if (in_erb_test_mode()) {
    int erb_test_done_count = 0;
    for (SrbpMap::iterator i = srbp_map_.begin(); i != srbp_map_.end(); i++) {
      i->second.TimerHandler();
      if (i->second.IsDelivered()) {
        erb_test_done_count++;
      }
    }

    if (erb_test_done_count == in_config_.erb_test_n) {
      FinishErbTest();
    }
  } else {
    erng_.TimerHandler();
    if (erng_.IsAccepted()) {
      FinishErng();
    }
  }
}

// TODO: not fully implemented
void Core::HandshakeHandler(PeerProfile &peer, rapidjson::Document &d) {
  using namespace rapidjson;
  // go check and intercept "pub" message to finish the handshake
  // also stop all other message before going on
  LockGuard lg(peer.mutex_);  // RAII lock

  std::string type = d["type"].GetString();
  FullAddress source = peer.address_;
  /* if peer is not handshaked with core before then initialize the shared
   * key first, i.e. handshake.
   */
  if (!peer.is_handshaked()) {
    if (type != "pub") {
      log_warn(source, "didn't handshake");
      return;
    }

    /* peer's identity should be verified at first via the remote attestation
     * service
     *
     * XXX: this stage is not implemented yet.
     */
    // ocall_verify_public_key(source ,identity)

    /*
     * start to handshake and generate the shared key
     */
    std::string pub = (d.HasMember("pub_key") && d["pub_key"].IsString())
                          ? d["pub_key"].GetString()
                          : "";
    log_trace(source, "#%s try to handshake using pub=%s", __func__,
              pub.c_str());

    if (!peer.ComputeShared(pub, dh_)) {
      log_error(source, "failed to compute shared key");
      return;
    }
    log_trace(source, "#%s shared_key compute succeeded", __func__);

    /*
     * send core's public key to the source (write out via channel)
     */
    char *core_pub_hex = BN_bn2hex(dh_->pub_key);
    log_trace(source, "#%s public key converted to hex %s", __func__,
              core_pub_hex);
    d["pub_key"].SetString(StringRef(core_pub_hex), d.GetAllocator());

    StringBuffer out_buf;
    Writer<StringBuffer> writer(out_buf);
    d.Accept(writer);

    // std::copy(out_buf.GetString(), out_buf.GetString() + out_buf.GetSize(),
    // channel);
    // log_info(source, "#%s channel write: %s", __func__, channel);
    OPENSSL_free(core_pub_hex);
    return;
  }
}

uint64_t Core::GetPeerSeq(uint64_t id) {
  // if (id == host_id_) {
  // return self_profile_.GetSeq();
  // }
  FullAddress fa = id_to_fa(id);
  if (IsInitialPeer(fa)) {
    return GetPeerProfile(fa).GetSeq();
  } else {
    return 0;
  }
}

void Core::IncPeerSeq(uint64_t id) {
  // if (id == host_id_) {
  // self_profile_.IncSeq();
  // return;
  // }

  FullAddress fa = id_to_fa(id);
  if (IsInitialPeer(fa)) {
    GetPeerProfile(fa).IncSeq();
  }
}

void Core::MulticastAll(std::string &channel) {
  using namespace rapidjson;
  Document d;

  if (IsHandshakeEnabled()) {
    for (int i = 0; i < initial_peers_.size() - 1; i++) {
      log_debug("before set secret");
      if (!GetPeerProfile(multicast_receiver_[i]).SetSecret(d, channel)) {
        log_error(multicast_receiver_[i], "<-- failed to set secret");
        continue;
      }
      log_debug("before write to buffer");
      StringBuffer d_buf;
      Writer<StringBuffer> writer(d_buf);
      d.Accept(writer);
      log_debug("after write to buffer");
      log_debug(multicast_receiver_[i], "<-- private unicast %s",
                d_buf.GetString());

      sgx_status_t ret =
          OcallMulticast(multicast_receiver_ + i, 1, d_buf.GetString());
      if (ret != SGX_SUCCESS) {
        return;
      }
    }
  } else {
    Document d;
    d.SetObject();
    d.AddMember("type", "data", d.GetAllocator());
    d.AddMember("secret", Value(channel.c_str(), d.GetAllocator()).Move(),
                d.GetAllocator());

    StringBuffer d_buf;
    Writer<StringBuffer> writer(d_buf);
    d.Accept(writer);

    sgx_status_t ret = OcallMulticast(
        multicast_receiver_, initial_peers_.size() - 1, d_buf.GetString());
    if (ret != SGX_SUCCESS) {
      return;
    }
  }
}

void Core::Multicast(std::string &channel) {
  using namespace rapidjson;
  if (IsHandshakeEnabled()) {
    if (in_erb_test_mode()) {
      MulticastAll(channel);
      // sgx_status_t ret = OcallMulticast(
      // multicast_receiver_, initial_peers_.size() - 1, d_buf.GetString());
      // if (ret != SGX_SUCCESS) {
      // log_error("Core::Multicast OcallMulticast failed");
      // return;
      // }
    } else {
      if (!s_chosen_size_) {
        /*
         * s_chosen_receiver has not been created yet
         */
        const ERNG::IdSet &chosen = erng_.GetChosen();
        s_chosen_size_ = chosen.size();
        s_chosen_receiver_ = new FullAddress[s_chosen_size_];
        int i = 0;
        for (ERNG::IdSet::const_iterator it = chosen.begin();
             it != chosen.end(); it++) {
          uint64_t id = *it;
          s_chosen_receiver_[i++] = id_to_fa(id);
        }
      }

      Document d;

      for (int i = 0; i < s_chosen_size_; i++) {
        GetPeerProfile(s_chosen_receiver_[i]).SetSecret(d, channel);

        StringBuffer d_buf;
        Writer<StringBuffer> writer(d_buf);
        d.Accept(writer);

        sgx_status_t ret =
            OcallMulticast(s_chosen_receiver_ + i, 1, d_buf.GetString());
        if (ret != SGX_SUCCESS) {
          log_error("Core::Multicast OcallMulticast failed");
          return;
        }
      }
    }
  } else {
    Document d;
    d.SetObject();
    d.AddMember("type", "data", d.GetAllocator());
    d.AddMember("secret", Value(channel.c_str(), d.GetAllocator()).Move(),
                d.GetAllocator());
    StringBuffer d_buf;
    Writer<StringBuffer> writer(d_buf);
    d.Accept(writer);

    if (in_erb_test_mode()) {
      sgx_status_t ret = OcallMulticast(
          multicast_receiver_, initial_peers_.size() - 1, d_buf.GetString());
      if (ret != SGX_SUCCESS) {
        log_error("Core::Multicast OcallMulticast failed");
        return;
      }
    } else {
      if (!s_chosen_size_) {
        /*
         * s_chosen_receiver has not been created yet
         */
        const ERNG::IdSet &chosen = erng_.GetChosen();
        s_chosen_size_ = chosen.size();
        s_chosen_receiver_ = new FullAddress[s_chosen_size_];
        int i = 0;
        for (ERNG::IdSet::const_iterator it = chosen.begin();
             it != chosen.end(); it++) {
          uint64_t id = *it;
          s_chosen_receiver_[i++] = id_to_fa(id);
        }
      }

      sgx_status_t ret =
          OcallMulticast(s_chosen_receiver_, s_chosen_size_, d_buf.GetString());
      if (ret != SGX_SUCCESS) {
        log_error("Core::Multicast OcallMulticast failed");
        return;
      }
    }
  }
}

void Core::Unicast(FullAddress receiver, char *channel) {
  using namespace rapidjson;
  // TODO: if handshake is enabled, secret needs symmetric encryption
  Document d;
  d.SetObject();
  d.AddMember("type", "data", d.GetAllocator());
  d.AddMember("secret", StringRef(channel), d.GetAllocator());

  StringBuffer d_buf;
  Writer<StringBuffer> writer(d_buf);
  d.Accept(writer);

  sgx_status_t ret = OcallMulticast(&receiver, 1, d_buf.GetString());
  if (ret != SGX_SUCCESS) {
    log_error("Core::Unicast OcallMulticast failed");
    return;
  }
}

void Core::StartErbTest() {
  /*
   *  initialize srbp instances for everyone
   */
  for (std::size_t i = 0; i < in_config_.erb_test_n; i++) {
    uint64_t peer_id = fa_to_id(fa_list_[i]);
    std::pair<SrbpMap::iterator, bool> result =
        srbp_map_.insert(std::make_pair(peer_id, SRBP(*this, peer_id)));
    result.first->second.SetShortInitId(fa_list_[i].port);
  }

  log_info("================================ ERB Test Start (%d/%d)",
           in_config_.erb_test_n, srbp_map_.size());

  uint64_t initiator = host_id_;
  uint64_t rand_message;
  sgx_status_t ret = sgx_read_rand(
      reinterpret_cast<unsigned char *>(&rand_message), sizeof(rand_message));
  if (ret != SGX_SUCCESS) {
    log_error("#%s error: unable to get random number from sgx", __func__);
    return;
  }

  // FIXME: use initiator id as rand_message for verfication simplicity
  rand_message = host_id_;  // remove this after test

  log_info("ERB[%llu].initiator message: %llu", host_id_, rand_message);

  std::auto_ptr<char> rand_buf(new char[sizeof(uint64_t)]);
  *reinterpret_cast<uint64_t *>(rand_buf.get()) = rand_message;

  std::auto_ptr<char> m_buf(new char[3 * sizeof(uint64_t)]);
  std::size_t len = util::b64_op(rand_buf.get(), sizeof(uint64_t), m_buf.get(),
                                 sizeof(uint64_t) * 3, util::kBase64Encode);
  *(m_buf.get() + len) = 0;

  std::string init_m(m_buf.get());

  // launch first {in_config_.erb_test_n} ERB instances
  // host_fa_ will be initiator
  for (std::size_t i = 0; i < in_config_.erb_test_n; i++) {
    SrbpMap::iterator erb = srbp_map_.find(fa_to_id(fa_list_[i]));

    if (erb == srbp_map_.end()) {
      log_error("ERB test error: can not find ERB[%llu]",
                fa_to_id(fa_list_[i]));
      return;
    }
    if (fa_list_[i] == host_fa_) {
      erb->second.InitiatorStart(GetCurrentTime(), init_m);
    } else {
      erb->second.NonInitiatorStart(GetCurrentTime());
    }
  }
}

void Core::FinishErbTest() {
  if (erb_test_finished_) {
    return;
  }
  erb_test_finished_ = true;
  log_info("================================ ERB Test Finish (%llus)",
           GetCurrentTime() - start_time_);
  uint64_t result = 0;

  for (std::size_t i = 0; i < in_config_.erb_test_n; i++) {
    SrbpMap::iterator erb = srbp_map_.find(fa_to_id(fa_list_[i]));
    std::string m = erb->second.GetM();
    uint64_t it_rand;

    std::auto_ptr<char> m_buf(new char[m.size()]);
    m.copy(m_buf.get(), m.size());
    util::b64_op(m_buf.get(), m.size(), reinterpret_cast<char *>(&it_rand),
                 sizeof(it_rand), util::kBase64Decode);

    log_info("ERB[%llu] result: %llu", erb->first, it_rand);
    result ^= it_rand;
  }
  uint64_t precomp_result = precompute_erb_test_result(in_config_.erb_test_n);
  log_info("ERB test precomputed result: %llu", precomp_result);
  std::string result_s = result == precomp_result ? "success" : "failure";
  log_info("ERB test %s", result_s.c_str());
  OcallFinish();
}

void Core::StartErng() { erng_.Start(GetCurrentTime()); }

void Core::FinishErng() {
  log_info("================================ ERNG Finish (%llus)",
           GetCurrentTime() - start_time_);
  log_critical("ERNG result rand: %llu", erng_.GetReliableRand());
}

int Core::MessageHandler(FullAddress source, rapidjson::Document &d) {
  using namespace rapidjson;

  std::string type = GetString(d, "type");
  if (type == "") {
    return EPH_FAILURE;
  }

  if (type == "pub") {
    // TODO: deal with handshake, not enabled for now
    if (IsHandshakeEnabled()) {
      PeerProfile &peer = GetPeerProfile(source);
      HandshakeHandler(peer, d);
      return EPH_SUCCESS;
    }
    return EPH_FAILURE;
  }

  if (type == "data") {
    std::string secret = GetString(d, "secret");
    if (secret == "") {
      return EPH_FAILURE;
    }

    Document secret_doc;  // secret document
    if (IsHandshakeEnabled()) {
      if (!GetPeerProfile(source).GetSecret(d, secret_doc)) {
        log_error("failed to parse encrypted secret doc");
        return EPH_FAILURE;
      }
    } else {
      secret_doc.Parse(GetString(d, "secret").c_str());
      if (secret_doc.HasParseError()) {
        log_error("failed to parse secret doc");
        return EPH_FAILURE;
      }
    }

    // log_debug(source, "--> #%s request secret: %s", __func__,
    // secret.c_str());

    if (in_erb_test_mode()) {
      // log_warn("In ERB Test Mode");
      /*
       * find the SRBP instance by msg.id() and handle it
       */
      SrbpMsg msg(secret_doc);
      SrbpMap::iterator srbp = srbp_map_.find(msg.id());
      if (srbp == srbp_map_.end()) {
        log_error(source, "can not find srbp instance for id %llu", msg.id());
        return EPH_FAILURE;
      } else if (srbp->second.Handler(source, msg) == EPH_FAILURE) {
        log_error(source, "srbp handler error");
        return EPH_FAILURE;
      }
    } else {
      // log_warn("In Normal ERNG Mode");
      /*
       * ERNG
       */
      ErngMsg msg(secret_doc);
      if (msg.type() == "CHOSEN" || msg.type() == "FINAL") {
        // log_info(source, "ERNG handler");
        if (erng_.Handler(source, msg) == EPH_FAILURE) {
          log_error(source, "ERNG handler error");
          return EPH_FAILURE;
        }
        secret_doc.SetObject();
      } else {
        // log_info(source, "ERB handler in ERNG");
        SrbpMsg msg(secret_doc);
        SrbpMap::iterator srbp = srbp_map_.find(msg.id());
        if (srbp == srbp_map_.end()) {
          log_error(source, "can not find srbp instance for id %llu", msg.id());
          return EPH_FAILURE;
        } else if (srbp->second.Handler(source, msg) == EPH_FAILURE) {
          log_error(source, "srbp handler error");
          return EPH_FAILURE;
        }
      }
    }

    // log_trace(source, "<-- #%s reply secret: %s", __func__,
    // secret_buf.GetString());
    if (IsHandshakeEnabled()) {
      if (!GetPeerProfile(source).SetSecret(d, secret_doc)) {
        return EPH_FAILURE;
      }
    } else {
      StringBuffer secret_buf;
      Writer<StringBuffer> writer(secret_buf);
      secret_doc.Accept(writer);
      Document::AllocatorType &da = d.GetAllocator();
      d.SetObject();
      d.AddMember("type", "data", da);
      d.AddMember("secret", Value(secret_buf.GetString(), da).Move(), da);
    }
  }

  return EPH_SUCCESS;
}

std::size_t Core::TotalNodes() {
  if (in_erb_test_mode()) {
    return initial_peers_.size();
  } else {
    return erng_.s_chosen_.size();
  }
}

uint64_t Core::GetCurrentTime() { return current_time_; }

uint64_t Core::GetUptime() { return current_time_ - start_time_; }

uint64_t Core::precompute_erb_test_result(int first_n) {
  uint64_t result = 0;

  for (std::size_t i = 0; i < first_n; i++) {
    result ^= fa_to_id(fa_list_[i]);
  }
  return result;
}

bool Core::in_erb_test_mode() { return in_config_.erb_test_mode; }

/*
 * PeerProfile
 */
const std::size_t PeerProfile::kAESKeyLen = 256 / 8;
const std::size_t PeerProfile::kAESIvLen = 128 / 8;

const unsigned char PeerProfile::iv_[16] = "123456789012345";

bool PeerProfile::ComputeShared(std::string pub_key, DH *core_dh) {
  if (handshaked_) {
    return true;
  }
  // recover the peer's public key from hex string
  BN_hex2bn(&dh_pub_key_, pub_key.c_str());

  std::size_t dh_shared_key_length = DH_size(core_dh);

  dh_shared_key_ = new unsigned char[DH_size(core_dh)];
  int size = DH_compute_key(dh_shared_key_, dh_pub_key_, core_dh);
  if (size == -1) {
    log_error(address_, "DH_compute_key failed");
    delete dh_shared_key_;
    return false;
  }

  // obtain aes_256_key from shared_key;

  if (dh_shared_key_length > kAESKeyLen) {
    std::copy(dh_shared_key_, dh_shared_key_ + kAESKeyLen, key_);
  } else {
    std::copy(dh_shared_key_, dh_shared_key_ + dh_shared_key_length, key_);
    std::fill(key_ + dh_shared_key_length, key_ + kAESKeyLen, 0);
  }

  // char sk_b64[1024] = {0};
  // p2p_sgx::util::b64_op((char *)shared_key_, size, sk_b64, 1024,
  // p2p_sgx::util::kBase64Encode);

  // log_debug(address_, "shared_key len=%d key(base64)=%s", size, sk_b64);

  handshaked_ = true;
  return true;
}

bool PeerProfile::Encrypt(unsigned char *plain_text, std::size_t plain_len,
                          unsigned char *cipher_text, std::size_t &cipher_len) {
  if (!handshaked_) {
    return false;
  }
  EVP_CIPHER_CTX *ctx;

  if (!(ctx = EVP_CIPHER_CTX_new())) {
    log_error("failed to create new EVP_CIPHER_CTX");
    return false;
  }

  // EVP_CIPHER_CTX_set_padding(ctx, 0);

  if (1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key_, iv_)) {
    log_error("failed to init encrypt");
    EVP_CIPHER_CTX_free(ctx);
    return false;
  }

  int len;

  if (1 != EVP_EncryptUpdate(ctx, cipher_text, &len, plain_text, plain_len)) {
    log_error("failed to update encrypt");
    EVP_CIPHER_CTX_free(ctx);
    return false;
  }

  cipher_len = len;

  if (1 != EVP_EncryptFinal_ex(ctx, cipher_text + len, &len)) {
    log_error("failed to finialize encrypt");
    EVP_CIPHER_CTX_free(ctx);
    return false;
  }

  cipher_len += len;

  EVP_CIPHER_CTX_free(ctx);

  return true;
}

bool PeerProfile::Decrypt(unsigned char *cipher_text, std::size_t cipher_len,
                          unsigned char *plain_text, std::size_t &plain_len) {
  if (!handshaked_) {
    return false;
  }

  EVP_CIPHER_CTX *ctx;

  if (!(ctx = EVP_CIPHER_CTX_new())) {
    log_error("failed to create new EVP_CIPHER_CTX");
    return false;
  }

  // EVP_CIPHER_CTX_set_padding(ctx, 0);

  if (1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key_, iv_)) {
    log_error("failed to init decrypt");
    EVP_CIPHER_CTX_free(ctx);
    return false;
  }

  int len;

  if (1 != EVP_DecryptUpdate(ctx, plain_text, &len, cipher_text, cipher_len)) {
    log_error("failed to update decrypt");
    EVP_CIPHER_CTX_free(ctx);
    return false;
  }
  plain_len = len;
  plain_text[len] = 0;
  log_debug("#%s parital plain_text = %s", __func__, (char *)plain_text);

  if (1 != EVP_DecryptFinal_ex(ctx, plain_text + len, &len)) {
    log_error("failed to finalize decrypt");
    EVP_CIPHER_CTX_free(ctx);
    return false;
  }
  plain_len += len;

  EVP_CIPHER_CTX_free(ctx);

  return true;
}

bool PeerProfile::SetSecret(rapidjson::Document &d, std::string &secret) {
  using namespace rapidjson;

  log_debug("#%s secret.size() = %d", __func__, secret.size());

  std::size_t cipher_buf_max =
      (secret.size() + kAESIvLen - 1) / kAESIvLen * kAESIvLen;
  std::auto_ptr<unsigned char> cipher_buf(new unsigned char[cipher_buf_max]);
  unsigned char *secret_cipher = cipher_buf.get();

  std::size_t cipher_len;
  if (!Encrypt((unsigned char *)secret.c_str(), secret.size(), secret_cipher,
               cipher_len)) {
    log_error("#%s failed to encrypt", __func__);
    return false;
  }
  log_debug("#%s cipher_len = %d", __func__, cipher_len);

  for (int i = 0; i < cipher_len; i += 16) {
    log_debug(
        "#%s %02x%02x%02x%02x%02x%02x%02x%02x - "
        "%02x%02x%02x%02x%02x%02x%02x%02x",
        __func__, secret_cipher[i], secret_cipher[i + 1], secret_cipher[i + 2],
        secret_cipher[i + 3], secret_cipher[i + 4], secret_cipher[i + 5],
        secret_cipher[i + 6], secret_cipher[i + 7], secret_cipher[i + 8],
        secret_cipher[i + 9], secret_cipher[i + 10], secret_cipher[i + 11],
        secret_cipher[i + 12], secret_cipher[i + 13], secret_cipher[i + 14],
        secret_cipher[i + 15]);
  }

  // secret_cipher[cipher_buf_max] = 0;

  // std::size_t base64_buf_max = cipher_len * 1.333;
  // std::auto_ptr<unsigned char> base64_buf(new unsigned char[base64_buf_max]);
  // char *base64_buf_ptr = (char *)base64_buf.get();

  // char *base64_buf = p2p_sgx::util::Base64Encode((char *)secret_cipher, cipher_len,
                                               // false);
  // do a base64 encoding to construct secret_string
  // std::size_t base64_len = p2p_sgx::util::b64_op(
  // (char *)secret_cipher, cipher_len, (char *)base64_buf.get(),
  // base64_buf_max, p2p_sgx::util::kBase64Encode);

  // base64_buf.get()[base64_len] = 0;
  int base64_len = Base64encode_len(cipher_len);
  std::auto_ptr<char> base64_buf(new char[base64_len + 1]);
  ::Base64encode(base64_buf.get(), (char *)secret_cipher, cipher_len);

  log_debug("#%s base64_len (calculated) = %d", __func__, base64_len);
  log_debug("#%s base64_buf = %s", __func__, base64_buf.get());

  Document::AllocatorType &da = d.GetAllocator();
  d.SetObject();
  d.AddMember("type", "data", da);
  d.AddMember("secret", Value((char *)base64_buf.get(), da).Move(), da);

  // StringBuffer temp_buf;
  // Writer<StringBuffer> writer(temp_buf);
  // d.Accept(writer);

  // log_debug("#%s finish d = %s", __func__, temp_buf.GetString());
  return true;
}

bool PeerProfile::SetSecret(rapidjson::Document &d, rapidjson::Document &sd) {
  using namespace rapidjson;
  StringBuffer secret_buf;
  Writer<StringBuffer> writer(secret_buf);
  sd.Accept(writer);

  std::string sd_string(secret_buf.GetString());
  return SetSecret(d, sd_string);

  // std::size_t cipher_buf_max =
  // (secret_buf.GetSize() + kAESIvLen - 1) / kAESIvLen * kAESIvLen;

  // std::auto_ptr<unsigned char> cipher_buf(new unsigned char[cipher_buf_max]);

  // unsigned char *secret_cipher = cipher_buf.get();

  // std::size_t cipher_len;
  // if (!Encrypt((unsigned char *)secret_buf.GetString(), secret_buf.GetSize(),
  // secret_cipher, cipher_len)) {
  // return false;
  // }

  // std::size_t base64_buf_max = cipher_len * 1.3;
  // std::auto_ptr<unsigned char> base64_buf(new unsigned char[base64_buf_max]);

  // // do a base64 encoding to construct secret_string
  // std::size_t base64_len = p2p_sgx::util::b64_op(
  // (char *)secret_cipher, cipher_len, (char *)base64_buf.get(),
  // base64_buf_max, p2p_sgx::util::kBase64Encode);
  // base64_buf.get()[base64_len] = 0;

  // Document::AllocatorType &da = d.GetAllocator();
  // d.SetObject();
  // d.AddMember("type", "data", da);
  // d.AddMember("secret", Value((char *)base64_buf.get(), da).Move(), da);
  // return true;
}

bool PeerProfile::GetSecret(rapidjson::Document &d, rapidjson::Document &sd) {
  std::string secret = GetString(d, "secret");

  log_debug("#%s secret = %s, length = %d", __func__, secret.c_str(), secret.size());

  int cipher_len = ::Base64decode_len(secret.c_str());

  std::auto_ptr<unsigned char> cipher_buf(new unsigned char[cipher_len + 1]);
  unsigned char *secret_cipher = cipher_buf.get();

  // std::fill(secret_cipher, secret_cipher + secret.size(), 0);

  int bytes_decoded = ::Base64decode((char *)secret_cipher, secret.c_str());
  log_debug("#%s bytes_decoded = %d", __func__, bytes_decoded);

  // do a base64 decoding to recover secret_cipher
  // std::size_t cipher_len = p2p_sgx::util::b64_op(
  // (char *)secret.c_str(), secret.size(), (char *)secret_cipher,
  // secret.size(), p2p_sgx::util::kBase64Decode);

  log_debug("#%s cipher_len = %d", __func__, cipher_len);
  // cipher_len = (cipher_len + kAESIvLen - 1) / kAESIvLen * kAESIvLen;
  // log_debug("#%s cipher_len = %d", __func__, cipher_len);

  for (int i = 0; i < bytes_decoded; i += 16) {
    log_debug(
        "#%s %02x%02x%02x%02x%02x%02x%02x%02x - "
        "%02x%02x%02x%02x%02x%02x%02x%02x",
        __func__, secret_cipher[i], secret_cipher[i + 1], secret_cipher[i + 2],
        secret_cipher[i + 3], secret_cipher[i + 4], secret_cipher[i + 5],
        secret_cipher[i + 6], secret_cipher[i + 7], secret_cipher[i + 8],
        secret_cipher[i + 9], secret_cipher[i + 10], secret_cipher[i + 11],
        secret_cipher[i + 12], secret_cipher[i + 13], secret_cipher[i + 14],
        secret_cipher[i + 15]);
  }
  std::auto_ptr<unsigned char> plain_buf(new unsigned char[cipher_len]);
  unsigned char *secret_plain = plain_buf.get();

  std::size_t plain_len;
  if (!Decrypt((unsigned char *)secret_cipher, bytes_decoded, secret_plain,
               plain_len)) {
    return false;
  }

  plain_buf.get()[plain_len] = 0;

  log_debug("#%s plain_buf = %s", __func__, plain_buf.get());

  sd.Parse((char *)secret_plain);
  return !sd.HasParseError();
}

uint64_t PeerProfile::GetSeq() { return seq_; }
uint64_t PeerProfile::IncSeq() { return ++seq_; }
void PeerProfile::SetSeq(uint64_t seq) { seq_ = seq; }
} /* end of namespace enclave */
} /* end of namespace p2p_sgx */
