#ifndef __ENCLAVE_LOG_H__
#define __ENCLAVE_LOG_H__

#include "user_type.h"

#define LOG_MAX_BUF_SIZE 1024

void log_error(const char *fmt_s, ...);
void log_warn(const char *fmt_s, ...);
void log_info(const char *fmt_s, ...);
void log_debug(const char *fmt_s, ...);
void log_trace(const char *fmt_s, ...);
void log_critical(const char *fmt_s, ...);

void log_error(FullAddress ad, const char *fmt_s, ...);
void log_warn(FullAddress ad, const char *fmt_s, ...);
void log_info(FullAddress ad, const char *fmt_s, ...);
void log_debug(FullAddress ad, const char *fmt_s, ...);
void log_trace(FullAddress ad, const char *fmt_s, ...);
void log_critical(FullAddress ad, const char *fmt_s, ...);

#define sgx_debug(A)                                                         \
  log_debug("Sgx error code: %x - in line %d of file %s (function %s)\n", A, \
            __LINE__, __FILE__, __func__)

#define CHECK_STATUS(status)     \
  do {                           \
    if (status != SGX_SUCCESS) { \
      sgx_debug(status);         \
      abort();                   \
    }                            \
  } while (0)

#endif /*  __ENCLAVE_LOG_H__ */
