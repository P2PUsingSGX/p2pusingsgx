#ifndef __CONFIG_H__
#define __CONFIG_H__

#include <string>
#include <vector>

#include "user_type.h"

namespace p2p_sgx {

struct Config {
  /* peer settings */
  std::string host_ip;
  std::string host_port;
  std::vector<std::string> peers;

  /* network tunning */
  int reconnect_wait_time;      // milliseconds
  double multicast_wait_ratio;  // ratio

  /* ERB parameters */
  int erb_round_time;        // seconds
  int erb_round_delay_time;  // seconds

  /* In ERB test, only first {erb_test_n} ERB instances will run */
  int erb_test_n;
  /* For ERB(1/n) byzantine simulation */
  int erb_test_byzantine_n;  // usuall this config will make erb_test_n be 1

  int erb_test_mode; // the mode only test ERB instances

  int erng_r1_max;
  int erng_r2_max;

  InternalConfig ToInternalConfig() {
    InternalConfig iconf;
    iconf.erb_round_time = erb_round_time;
    iconf.erb_round_delay_time = erb_round_delay_time;
    iconf.erb_test_n = erb_test_n;
    iconf.erb_test_mode = erb_test_mode;
    iconf.erng_r1_max = erng_r1_max;
    iconf.erng_r2_max = erng_r2_max;
    return iconf;
  }
};

} /* end of namespace */

#endif /* __CONFIG_H__ */
