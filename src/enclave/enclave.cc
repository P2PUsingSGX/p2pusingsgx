#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

#include "enclave.h"
#include "core.h"
#include "enclave_t.h"
#include "user_type.h"
#include "util.h"

/*
 * root storage of all things in enclave
 */
p2p_sgx::enclave::Core *global_core;

/*
 *
 * Ecalls
 *  - EcallInitializeCore
 *  - EcallPeerHandler
 *  - EcallSrbpInit
 *  - EcallTimer
 *
 */

void EcallInitializeCore(FullAddress address_list[], size_t count,
                         FullAddress host, InternalConfig iconf) {
  using namespace p2p_sgx::enclave;

  global_core = new Core(host, address_list, count, iconf);
}

void EcallTimer() {
  log_trace("#%s", __func__);
  global_core->Update();
}

int error_return(char *channel, int channel_capacity) {
  std::fill(channel, channel + channel_capacity, 0);
  return EPH_FAILURE;
}

// FIXME: channel is used for both input and output. This means output data must
// be shorter than input. The channel_capacity is now set as CHANNEL_MIN_SIZE
// if any error happens, clear the channel and return error;
int EcallPeerHandler(FullAddress source, char *channel, int channel_capacity) {
  using namespace p2p_sgx::enclave;
  using namespace rapidjson;
  log_trace(source, "#%s channel read: %s", __func__, channel);

  /* only allow peer who is in the initial group */
  if (!global_core->IsInitialPeer(source)) {
    log_error(source, "is not in initial peer set. Stop EcallPeerHandler");
    return error_return(channel, channel_capacity);
  }

  /*
   * read from channel
   */
  Document d;
  d.Parse(channel);
  if (d.HasParseError()) {
    log_error(source, "#%s json parser error", __func__);
    return error_return(channel, channel_capacity);
  }

  /*
   * call message handler
   */
  if (global_core->MessageHandler(source, d) == EPH_FAILURE) {
    return error_return(channel, channel_capacity);
  }

  /*
   * write to channel
   */
  StringBuffer out_buf;
  Writer<StringBuffer> writer(out_buf);
  d.Accept(writer);

  std::size_t len = out_buf.GetSize() + 1;

  if (len > channel_capacity) {
    log_error(
        "output channel (%d) is larger than channel_capacity (%d). Output "
        "message will be truncated",
        len, channel_capacity);
  }

  std::copy(out_buf.GetString(), out_buf.GetString() + channel_capacity,
            channel);

  log_trace(source, "#%s channel write: %s", __func__, channel);
  return EPH_SUCCESS;
}

void EcallStartCore() {
  // global_core->StartSrbpDummy();
  // global_core->StartStrawman();
}

namespace p2p_sgx {
namespace enclave {

/*
 * Utility
 */
std::string GetString(rapidjson::Document &d, const char *key) {
  return d.IsObject() && d.HasMember(key) && d[key].IsString()
             ? d[key].GetString()
             : "";
}

std::string base64_sha256(char *str) {
  // log_debug("#%s input: %s", __func__, str);
  sgx_sha256_hash_t p_hash;

  sgx_status_t ret;
  ret = sgx_sha256_msg(reinterpret_cast<uint8_t *>(str), strlen(str), &p_hash);
  if (ret != SGX_SUCCESS) {
    log_error("sgx_sha256_msg failed");
    return "";
  }

  char base64[64];
  int size = p2p_sgx::util::b64_op(reinterpret_cast<char *>(p_hash), 32, base64,
                                   64, p2p_sgx::util::kBase64Encode);
  base64[size] = 0;
  // log_debug("#%s result(len=%d): %s", __func__, size, base64);
  return base64;
}

FullAddress &id_to_fa(uint64_t &id) {
  return *reinterpret_cast<FullAddress *>(&id);
}

uint64_t fa_to_id(FullAddress &fa) {
  return *reinterpret_cast<uint64_t *>(&fa);
}

void print_json(rapidjson::Document &d) {
  std::auto_ptr<char> ch(p2p_sgx::util::NewBufferFromJson(d));
  log_debug("JSON: %s", ch.get());
}

std::auto_ptr<char> NewAutoPtrFromJson(rapidjson::Document &d) {
  return std::auto_ptr<char>(util::NewBufferFromJson(d));
}

} /* end of namespace enclave */
} /* end of namespace p2p_sgx */
