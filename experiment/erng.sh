#!/bin/bash

log_root=./log
time_path=`date +"%Y%m%d-%H%M%S"`
log_dir=$log_root/$time_path

run() {
  mkdir -p $log_dir
  echo $log_dir > current_log_path

  n=$1
  round=$2
  delay=$3

  echo $n nodes running
  echo log path: $log_dir

  for i in `seq $n`
  do
    # if [[ $i -ne $n ]]
    # then
    ./p2psgx -l1 -t$i/$n -r $round -d $delay -m 0 -v 1> $log_dir/${i}_${n}.log 2>&1 &
    # fi
  done
}

usage() {
  echo "Usage: $1 n round delay "
}


if [ -z $1 ] || [ -z $2 ] || [ -z $3 ]
then
  usage $0
  exit 1
fi

run $1 $2 $3

