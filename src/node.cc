#include <execinfo.h>
#include <boost/bind.hpp>
#include <chrono>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <sstream>
#include <thread>

#include "message.pb.h"
#include "node.h"

namespace p2p_sgx {

using boost_tcp = boost::asio::ip::tcp;

Node::Node(Config &config)
    : io_service_(),
      signals_(io_service_),
      socket_(io_service_),
      acceptor_(io_service_),
      timer_(io_service_, boost::posix_time::seconds(1)),
      config_(config),
      brain_(*this, config) {
  /*
   * Initialize timer
   */
  timer_.async_wait(boost::bind(&Node::AsyncTimer, this));

  /*
   * set connection parameters
   */
  Connection::kStartDelay =
      config.multicast_wait_ratio * 1000 *
      (double)(config.erb_round_time - config.erb_round_delay_time);
  Connection::kReconnectWait = config.reconnect_wait_time;

  /*
   * handle three SIG* signals gracefully
   */
  signals_.add(SIGINT);
  signals_.add(SIGTERM);
  signals_.add(SIGQUIT);
  // signals_.add(SIGSEGV);
  signals_.async_wait([this](boost::system::error_code, int sig_num) {
    static std::map<int, std::string> sig_name{{SIGINT, "SIGINT"},
                                               // {SIGSEGV, "SIGSEGV"},
                                               {SIGTERM, "SIGTERM"},
                                               {SIGQUIT, "SIGQUIT"}};

    // if (sig_num == SIGSEGV) {
    // void *array[10];
    // size_t size;

    // size = backtrace(array, 10);

    // // print out all the frames to stderr
    // fprintf(stderr, "SEGMENTATION fault\n");
    // backtrace_symbols_fd(array, size, STDERR_FILENO);
    // }
    if (sig_name.find(sig_num) != sig_name.end()) {
      log->debug("signal {} triggered", sig_name[sig_num]);
      log->trace("signal {} triggered", sig_name[sig_num]);
    } else {
      log->error("unknown signal triggered: {}", sig_num);
    }

    log->info("exiting...");
    Stop();
  });
  log->trace("Node signals are set");

  /*
   * bind endpoint_ to acceptor_
   */
  boost_tcp::resolver resolver(io_service_);

  auto resolver_iterator =
      resolver.resolve({config.host_ip, config_.host_port});
  auto hostname = resolver_iterator->host_name();
  auto endpoint = resolver_iterator->endpoint();

  acceptor_.open(endpoint.protocol());
  acceptor_.set_option(boost_tcp::acceptor::reuse_address(true));
  acceptor_.bind(endpoint);
  acceptor_.listen();

  log->info("node is listening at {}:{}", endpoint.address().to_string(),
            endpoint.port());

  AsyncAccept();

  // set byzantine to be the next one in the peer list
  auto self_pos = std::find(config.peers.begin(), config.peers.end(),
                            config.host_ip + ":" + config.host_port);
  if (self_pos != config.peers.end() && self_pos + 1 != config.peers.end()) {
    next_byzantine_ = *(self_pos + 1);
    is_byzantine_ =
        (self_pos - config.peers.begin()) < config.erb_test_byzantine_n;
  }
}

void Node::Run() {
  log->trace("node is running");
  brain_.Start();
  // code before this will be run
  io_service_.run();  // will block here normally
  log->warn("io_service stops running");
}

void Node::Stop() {
  log->trace("node is stopping");
  io_service_.stop();
  log->trace("node is stopped (io_service stopped)");
}

void Node::AsyncAccept() {
  acceptor_.async_accept(socket_, [this](boost::system::error_code ec) {
    // make sure the server is still running
    if (!acceptor_.is_open()) {
      conn_log->error("AsyncAccept exits due to the closed acceptor");
      return;
    }

    if (ec) {
      conn_log->error("AsyncAccept error: {}", ec.message());
      socket_.close();
    } else {
      auto ip = socket_.remote_endpoint().address().to_string();
      auto port = socket_.remote_endpoint().port();
      log->trace("AsyncAccept new remote endpoint: {}:{}", ip, port);

      // save the connection and handle it
      auto new_conn = std::make_shared<Connection>(std::move(socket_), *this);
      InsertConnection(new_conn);
      new_conn->Start();
    }

    // std::this_thread::sleep_for(std::chrono::milliseconds(1));
    // handle next incoming connection
    AsyncAccept();
  });
}

// TODO
void Node::Send(std::string addr, PeerMessage msg) {
  // 1. find the connection used to send
  //
  //
  //    1.1 if not found, create a new one
  //
  //
  // 2. directly call Send();
  auto conn_it = sending_conns_.find(addr);
  bool in{true};
  if (conn_it == sending_conns_.end()) {
    auto new_conn = std::make_shared<Connection>(addr, *this);
    std::tie(conn_it, in) =
        sending_conns_.insert(std::make_pair(addr, new_conn));
  }

  if (in) {
    conn_it->second->Send(msg);
  } else {
    conn_log->error("{} #{} Can not insert into ConnectionMap", addr, __func__);
  }
}

void Node::Send(std::string addr, PeerMessage msg, std::size_t ms) {
  // non-zero value means this is enabled and message will only be sent to
  // next byzantine node
  if (config_.erb_test_byzantine_n > 0 && is_byzantine_) {
    if (addr != next_byzantine_) {
      return;
    }
  }

  auto new_send =
      std::make_shared<SendDelayer>(*this, addr, std::move(msg), ms);
  timers_.insert(new_send);
}
/*
 *
 * void Node::AsyncConnect(std::string addr, PeerMessage msg) {
 *   auto ip = addr.substr(0, addr.find_first_of(':'));
 *   auto port = addr.substr(addr.find_last_of(':') + 1);
 *
 *   boost_tcp::resolver resolver{io_service_};
 *   boost_tcp::socket sock{io_service_};
 *
 *   auto endpointer_it = resolver.resolve({ip, port});
 *
 *   auto new_conn =
 *       std::make_shared<Connection>(std::move(sock), endpointer_it, msg,
 * *this);
 *   InsertConnection(new_conn);
 *   new_conn->Start();
 * }
 *
 */
void Node::InsertConnection(std::shared_ptr<Connection> c) {
  SPDLOG_TRACE(log, "InsertConnection");
  connections_.insert(c);
}

void Node::DeleteConnection(std::shared_ptr<Connection> c) {
  SPDLOG_TRACE(log, "DeleteConnection");
  connections_.erase(c);
}

void Node::AsyncTimer() {
  timer_.expires_at(timer_.expires_at() + boost::posix_time::seconds(1));
  timer_.async_wait(boost::bind(&Node::AsyncTimer, this));
  log->debug("connection usage: listen:{} send:{}", connections_.size(),
             sending_conns_.size());
  brain_.Update();
}

void Node::PrintReport() {
  log->debug("message count: in: {} out: {}", message_in_, message_out_);
  log->debug("message bytes: tx: {} rx: {}", message_tx_, message_rx_);
}
} /* end of namespace p2p_sgx */
