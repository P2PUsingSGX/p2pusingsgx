#include <algorithm>
#include <cstdarg>
#include <cstdio>
#include <memory>
#include <string>

#include "log.h"
#include "enclave_t.h"

void log_error(const char *fmt_s, ...) {
  char buf[LOG_MAX_BUF_SIZE];

  va_list args;
  va_start(args, fmt_s);
  vsnprintf(buf, LOG_MAX_BUF_SIZE, fmt_s, args);
  va_end(args);

  enclave_logger(2, buf);
}

void log_error(FullAddress ad, const char *fmt_s, ...) {
  char ad_buf[22];
  char buf[LOG_MAX_BUF_SIZE];
  struct IpSeg {
    uint8_t s1, s2, s3, s4;
  };

  IpSeg *is = reinterpret_cast<IpSeg *>(&ad.ip);
  snprintf(ad_buf, 22, "%d.%d.%d.%d:%d", is->s1, is->s2, is->s3, is->s4,
           ad.port);
  std::string new_fmt = std::string(ad_buf) + " " + std::string(fmt_s);

  va_list args;
  va_start(args, fmt_s);
  vsnprintf(buf, LOG_MAX_BUF_SIZE, new_fmt.c_str(), args);
  va_end(args);

  enclave_logger(2, buf);
}

void log_warn(const char *fmt_s, ...) {
  char buf[LOG_MAX_BUF_SIZE];

  va_list args;
  va_start(args, fmt_s);
  vsnprintf(buf, LOG_MAX_BUF_SIZE, fmt_s, args);
  va_end(args);

  enclave_logger(1, buf);
}

void log_warn(FullAddress ad, const char *fmt_s, ...) {
  char ad_buf[22];
  char buf[LOG_MAX_BUF_SIZE];
  struct IpSeg {
    uint8_t s1, s2, s3, s4;
  };

  IpSeg *is = reinterpret_cast<IpSeg *>(&ad.ip);
  snprintf(ad_buf, 22, "%d.%d.%d.%d:%d", is->s1, is->s2, is->s3, is->s4,
           ad.port);
  std::string new_fmt = std::string(ad_buf) + " " + std::string(fmt_s);

  va_list args;
  va_start(args, fmt_s);
  vsnprintf(buf, LOG_MAX_BUF_SIZE, new_fmt.c_str(), args);
  va_end(args);

  enclave_logger(1, buf);
}

void log_info(const char *fmt_s, ...) {
  char buf[LOG_MAX_BUF_SIZE];

  va_list args;
  va_start(args, fmt_s);
  vsnprintf(buf, LOG_MAX_BUF_SIZE, fmt_s, args);
  va_end(args);

  enclave_logger(0, buf);
}

void log_info(FullAddress ad, const char *fmt_s, ...) {
  char ad_buf[22];
  char buf[LOG_MAX_BUF_SIZE];
  struct IpSeg {
    uint8_t s1, s2, s3, s4;
  };

  IpSeg *is = reinterpret_cast<IpSeg *>(&ad.ip);
  snprintf(ad_buf, 22, "%d.%d.%d.%d:%d", is->s1, is->s2, is->s3, is->s4,
           ad.port);
  std::string new_fmt = std::string(ad_buf) + " " + std::string(fmt_s);

  va_list args;
  va_start(args, fmt_s);
  vsnprintf(buf, LOG_MAX_BUF_SIZE, new_fmt.c_str(), args);
  va_end(args);

  enclave_logger(0, buf);
}

void log_debug(const char *fmt_s, ...) {
  char buf[LOG_MAX_BUF_SIZE];

  va_list args;
  va_start(args, fmt_s);
  vsnprintf(buf, LOG_MAX_BUF_SIZE, fmt_s, args);
  va_end(args);

  enclave_logger(-1, buf);
}

void log_debug(FullAddress ad, const char *fmt_s, ...) {
  char ad_buf[22];
  char buf[LOG_MAX_BUF_SIZE];
  struct IpSeg {
    uint8_t s1, s2, s3, s4;
  };

  IpSeg *is = reinterpret_cast<IpSeg *>(&ad.ip);
  snprintf(ad_buf, 22, "%d.%d.%d.%d:%d", is->s1, is->s2, is->s3, is->s4,
           ad.port);
  std::string new_fmt = std::string(ad_buf) + " " + std::string(fmt_s);

  va_list args;
  va_start(args, fmt_s);
  vsnprintf(buf, LOG_MAX_BUF_SIZE, new_fmt.c_str(), args);
  va_end(args);

  enclave_logger(-1, buf);
}

void log_trace(const char *fmt_s, ...) {
  char buf[LOG_MAX_BUF_SIZE];

  va_list args;
  va_start(args, fmt_s);
  vsnprintf(buf, LOG_MAX_BUF_SIZE, fmt_s, args);
  va_end(args);

  enclave_logger(-2, buf);
}

void log_trace(FullAddress ad, const char *fmt_s, ...) {
  char ad_buf[22];
  char buf[LOG_MAX_BUF_SIZE];
  struct IpSeg {
    uint8_t s1, s2, s3, s4;
  };

  IpSeg *is = reinterpret_cast<IpSeg *>(&ad.ip);
  snprintf(ad_buf, 22, "%d.%d.%d.%d:%d", is->s1, is->s2, is->s3, is->s4,
           ad.port);
  std::string new_fmt = std::string(ad_buf) + " " + std::string(fmt_s);

  va_list args;
  va_start(args, fmt_s);
  vsnprintf(buf, LOG_MAX_BUF_SIZE, new_fmt.c_str(), args);
  va_end(args);

  enclave_logger(-2, buf);
}

void log_critical(const char *fmt_s, ...) {
  char buf[LOG_MAX_BUF_SIZE];

  va_list args;
  va_start(args, fmt_s);
  vsnprintf(buf, LOG_MAX_BUF_SIZE, fmt_s, args);
  va_end(args);

  enclave_logger(-3, buf);
}

void log_critical(FullAddress ad, const char *fmt_s, ...) {
  char ad_buf[22];
  char buf[LOG_MAX_BUF_SIZE];
  struct IpSeg {
    uint8_t s1, s2, s3, s4;
  };

  IpSeg *is = reinterpret_cast<IpSeg *>(&ad.ip);
  snprintf(ad_buf, 22, "%d.%d.%d.%d:%d", is->s1, is->s2, is->s3, is->s4,
           ad.port);
  std::string new_fmt = std::string(ad_buf) + " " + std::string(fmt_s);

  va_list args;
  va_start(args, fmt_s);
  vsnprintf(buf, LOG_MAX_BUF_SIZE, new_fmt.c_str(), args);
  va_end(args);

  enclave_logger(-3, buf);
}
