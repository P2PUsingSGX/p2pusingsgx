#include <google/protobuf/io/coded_stream.h>
#include <boost/bind.hpp>

#include "brain.h"
#include "connection.h"
#include "message.pb.h"
#include "node.h"

namespace p2p_sgx {

using boost_error = boost::system::error_code;
using boost_tcp = boost::asio::ip::tcp;

std::size_t Connection::kStartDelay{0};
std::size_t Connection::kReconnectWait{0};

const std::size_t Connection::kLengthBytes{4};
const std::size_t Connection::kIOBufSize{8192};

Connection::Connection(boost_tcp::socket socket, Node& node)
    : socket_(std::move(socket)),
      node_(node),
      remote_record_valid_(false),
      local_record_valid_(false),
      timer_(node.io_service_),
      sending_(false) {
  node_.listen_connection_count++;
  debug_log("New listening connection {}", node_.listen_connection_count);
}
/*
 *
 * boost_tcp::endpoint addr_to_endpoint(std::string& addr) {
 *   auto ip = addr.substr(0, addr.find_first_of(':'));
 *   auto port = addr.substr(addr.find_last_of(':') + 1);
 *   std::size_t port_num;
 *   std::stoul(port, &port_num, 10);  // FIXME: not catch any exception
 *
 *   boost_tcp::endpoint
 * ep(boost::asio::ip::address_v4::from_string(ip.c_str()),
 *                          port_num);
 * }
 *
 */
Connection::Connection(std::string& receiver, Node& node)
    : node_(node),
      socket_(node.io_service_),  // not used
      timer_(node.io_service_),
      remote_record_valid_(false),
      local_record_valid_(false),
      sending_(true),
      sending_socket_() {
  auto ip = receiver.substr(0, receiver.find_first_of(':'));
  auto port = receiver.substr(receiver.find_last_of(':') + 1);

  boost_tcp::resolver resolver{node.io_service_};
  endpoint_it_ = resolver.resolve({ip, port});

  node_.send_connection_count++;
  debug_log("New sending connection {}", node_.send_connection_count);
}

Connection::~Connection() {
  if (sending_) {
    node_.send_connection_count--;
    debug_log("Destroy sending connection {}", node_.send_connection_count);
  } else {
    node_.listen_connection_count--;
    debug_log("Destroy listening connection {}", node_.listen_connection_count);
  }
  SPDLOG_TRACE(conn_log, "~Connection");
}

void Connection::Start() {
  SPDLOG_TRACE(conn_log, "Connection starts");
  if (sending_) {
    // double rand = (double)std::rand() / RAND_MAX;
    // std::size_t delay = kStartDelay * rand;
    // SPDLOG_DEBUG(conn_log, "connection start delay {}", delay);
    // timer_.expires_from_now(boost::posix_time::milliseconds(delay));
    // timer_.async_wait(boost::bind(&Connection::AsyncConnect, this));
  } else {
    AsyncRead();
  }
}

void Connection::CloseSending() {
  remote_record_valid_ = false;
  boost::system::error_code ec;
  sending_socket_.get()->shutdown(boost::asio::ip::tcp::socket::shutdown_both,
                                  ec);
  sending_socket_.get()->close();
  sending_socket_.reset();
}

void Connection::Terminate() {
  boost::system::error_code ec;
  socket_.shutdown(boost::asio::ip::tcp::socket::shutdown_both, ec);
  socket_.close();
  node_.DeleteConnection(shared_from_this());
}

void Connection::Send(PeerMessage msg) {
  if (!sending_socket_) {
    sending_socket_.reset(new boost_tcp::socket(node_.io_service_));
    boost::asio::async_connect(*sending_socket_.get(), endpoint_it_,
                               [this, msg](boost::system::error_code ec,
                                           boost_tcp::resolver::iterator) {
                                 if (ec) {
                                   error("AsyncConnect", ec.message());
                                   CloseSending();
                                   return;
                                   // socket_.close();
                                   // if (--reconnect_count_ < 0) {
                                   // Terminate();
                                   // return;
                                   // }
                                   // SPDLOG_DEBUG(conn_log, "AsyncConnect {}
                                   // reconnect({}) after {}
                                   // ms",
                                   // endpoint_it_->endpoint().port(),
                                   // reconnect_count_,
                                   // kReconnectWait);
                                   // timer_.expires_from_now(
                                   // boost::posix_time::milliseconds(kReconnectWait));
                                   // timer_.async_wait(boost::bind(&Connection::AsyncConnect,
                                   // this));
                                   // return;
                                 } else {
                                   AsyncSend(std::move(msg));
                                   AsyncReceive();  // start a AsyncReceive loop
                                 }
                               });

  } else {
    AsyncSend(std::move(msg));
    // do not start receive loop
  }
}

void Connection::AsyncRead() { AsyncReadLength(); }

void Connection::AsyncReadLength() {
  auto buf = ibuf_.prepare(kLengthBytes);

  boost::asio::async_read(
      socket_, boost::asio::buffer(buf, kLengthBytes),
      [this](boost_error ec, std::size_t bytes) {
        if (ec) {
          error("AsyncReadLength", ec.message());
          Terminate();
          return;
        }

        SPDLOG_TRACE(conn_log, "AsyncReadLength commit bytes: {}", bytes);
        ibuf_.commit(bytes);
        global_node->message_rx_ += bytes;
        uint32_t body_size;
        if (!unpack_msg_length(body_size)) {
          if (ibuf_.size() > kLengthBytes) {
            /*
             * if enough bytes have been read but no length can be unpacked
             */
            error("AsyncReadLength", "Message length not found");
            Terminate();
            return;
          } else {
            /*
             * maybe read more on length
             */
            AsyncReadLength();
            return;
          }
        }

        imsg_.Clear();  // clear protobuf message before read
        omsg_.Clear();
        AsyncReadBody(body_size);
      });
}

void Connection::AsyncReadBody(std::size_t size) {
  auto buf = ibuf_.prepare(size);

  // only call the handler when buf is full or an error occurs
  boost::asio::async_read(
      socket_, boost::asio::buffer(buf, size),
      [this, size](boost_error ec, std::size_t bytes) {
        if (ec) {
          error("AsyncReadBody", ec.message());
          Terminate();
          return;
        }
        SPDLOG_TRACE(conn_log, "AsyncReadBody bytes: {}, size:{}", bytes, size);
        ibuf_.commit(bytes);
        global_node->message_rx_ += bytes;

        if (!unpack_msg_body()) {
          error("AsyncReadBody", "Message body not found");
          Terminate();
          return;
        }

        Dispatch();
      });
}

void Connection::AsyncWrite() {
  if (obuf_.size() > kIOBufSize) {
    conn_log->debug("AsyncWrite obuf overflow ({}>{})", obuf_.size(),
                    kIOBufSize);
    error(__func__, "AsyncWrite obuf overflowed");
    Terminate();
    return;
  }

  if (obuf_.size() == 0) {
    SPDLOG_TRACE(conn_log, "{}:{} AsyncWrite obuf exhausted", remote_ip_,
                 remote_port_);
    SPDLOG_TRACE(conn_log, "AsyncWrite obuf exhausted");
    // In keep-connection mode, not terminate while obuf is exhausted
    // Terminate();
    // In instead, do a AsyncRead
    AsyncRead();
    return;
  }

  SPDLOG_TRACE(conn_log, "AsyncWrite");
  auto buf = obuf_.data();
  socket_.async_write_some(buf, [this](boost_error ec, std::size_t bytes) {
    if (ec) {
      error("AsyncWrite", ec.message());
      Terminate();
      return;
    }

    SPDLOG_TRACE(conn_log, "AsyncWrite bytes: {}", bytes);
    obuf_.consume(bytes);
    global_node->message_tx_ += bytes;

    AsyncWrite();
  });
}

void Connection::Dispatch() {
  global_node->message_in_++;
  SPDLOG_TRACE(conn_log, "Dispatch inspect request:{}",
               imsg_.ShortDebugString());
  node_.brain_.PeerHandler(imsg_, omsg_);

  set_origin(omsg_);

  SPDLOG_TRACE(conn_log, "Dispatch inspect reply:{}", omsg_.ShortDebugString());

  if (!pack_msg()) {
    error(__func__, "unable to pack_msg");
    Terminate();
    return;
  }

  AsyncWrite();
  global_node->message_out_++;
}

/*
 *    SENDER                                RECEIVER
 *
 *  AsyncSend   ------------------------>   AsyncRead  --+
 *                                                       | PeerHandler
 *  AsyncReceive  <----------------------   AsyncWrite --+
 *      |
 *  PeerHandler (half)
 *
 */

void Connection::AsyncSend(PeerMessage msg) {
  // msg -> omsg_
  omsg_ = std::move(msg);

  auto shared_obuf =
      std::shared_ptr<boost::asio::streambuf>(new boost::asio::streambuf);

  obuf_set_.insert(shared_obuf);
  set_origin(omsg_);

  // omsg_ -> shared_obuf
  if (!pack_msg(*shared_obuf.get())) {
    error("AsyncSend", "unable to pack message");
    return;
  }

  SPDLOG_TRACE(conn_log, "AsyncSend");

  auto buf = shared_obuf->data();
  // conn_log->debug("#{} sending_socket:{} msg:{} shared_obuf:{}, size: {}",
  // __func__, (void*)sending_socket_.get(),
  // msg.ShortDebugString(), (void*)shared_obuf.get(),
  // shared_obuf->size());

  boost::asio::async_write(
      *sending_socket_.get(), boost::asio::buffer(buf, shared_obuf->size()),
      [this, shared_obuf](const boost_error& ec, std::size_t bytes) {
        // we use boost::asio::write, which means this handler
        // will be called when
        // obuf_ is fully sent to receiver or an error occurs
        if (ec) {
          error("AsyncSend", ec.message());
          obuf_set_.erase(shared_obuf);
          CloseSending();
          return;
        }

        global_node->message_out_++;
        SPDLOG_TRACE(conn_log, "AsyncSend bytes: {}", bytes);
        shared_obuf->consume(bytes);
        global_node->message_tx_ += bytes;
        // AsyncReceive(); // not do receive here
      });
}

void Connection::AsyncReceive() {
  if (!sending_socket_) {
    conn_log->error("#{} sending_socket empty", __func__);
    return;
  }
  AsyncReceiveLength();
}

void Connection::AsyncReceiveLength() {
  auto buf = ibuf_.prepare(kLengthBytes);

  boost::asio::async_read(
      *sending_socket_.get(), boost::asio::buffer(buf, kLengthBytes),
      [this](boost_error ec, std::size_t bytes) {
        // socket_.async_read_some(buf, [this](boost_error ec,
        // std::size_t bytes) {
        if (ec) {
          error("AsyncReceiveLength", ec.message());
          CloseSending();
          return;
        }

        SPDLOG_TRACE(conn_log, "AsyncReceiveLength bytes: {}", bytes);

        ibuf_.commit(bytes);
        global_node->message_rx_ += bytes;

        uint32_t body_size;
        if (!unpack_msg_length(body_size)) {
          if (ibuf_.size() > kLengthBytes) {
            /*
             * if enough bytes have been read but no length can be unpacked
             */

            error("AsyncReceiveLength", "Message length not found");
            CloseSending();
            return;
          } else {
            /*
             * maybe read more on length
             */
            AsyncReceiveLength();
            return;
          }
        }

        imsg_.Clear();  // clear protobuf message before read
        omsg_.Clear();
        AsyncReceiveBody(body_size);
      });
}

void Connection::AsyncReceiveBody(std::size_t size) {
  SPDLOG_TRACE(conn_log, "#{} body size {}", __func__, size);
  auto buf = ibuf_.prepare(size);  // the exact size

  // only call the handler when buf is full or an error occurs
  boost::asio::async_read(
      *sending_socket_.get(), boost::asio::buffer(buf, size),
      [this, size](boost_error ec, std::size_t bytes) {
        if (ec) {
          error("AsyncReceiveBody", ec.message());
          CloseSending();
          return;
        }
        SPDLOG_TRACE(conn_log, "AsyncReceiveBody bytes: {}", bytes);
        ibuf_.commit(bytes);
        global_node->message_rx_+=bytes;

        if (!unpack_msg_body()) {
          error("AsyncReceiveBody", "Message body not found");

          CloseSending();
          return;
        }

        global_node->message_in_++;
        node_.brain_.PeerHandler(imsg_, omsg_);  // here omsg_ will be ignored
        imsg_.Clear();
        omsg_.Clear();  // explicitly clear omsg_

        AsyncReceive();  // receive next message
      });
}

bool Connection::pack_msg() {
  return pack_msg(obuf_);
  // std::ostream os(&obuf_);
  // google::protobuf::io::OstreamOutputStream oos(&os);
  // google::protobuf::io::CodedOutputStream cos(&oos);
  // SPDLOG_TRACE(conn_log, "pack_msg varint is: {}", omsg_.ByteSize());
  // cos.WriteLittleEndian32(omsg_.ByteSize());
  // // cos.WriteVarint64(omsg_.ByteSize());
  // return omsg_.SerializeToCodedStream(&cos);
}

bool Connection::pack_msg(boost::asio::streambuf& obuf) {
  std::ostream os(&obuf);
  google::protobuf::io::OstreamOutputStream oos(&os);
  google::protobuf::io::CodedOutputStream cos(&oos);
  SPDLOG_TRACE(conn_log, "pack_msg varint is: {}", omsg_.ByteSize());
  cos.WriteLittleEndian32(omsg_.ByteSize());
  // cos.WriteVarint64(omsg_.ByteSize());
  return omsg_.SerializeToCodedStream(&cos);
}

// FIXME: refactor before use it
// bool Connection::unpack_msg() {
// std::istream is(&ibuf_);
// google::protobuf::io::IstreamInputStream iis(&is);
// google::protobuf::io::CodedInputStream cis(&iis);
// uint32_t size;
// return cis.ReadLittleEndian32(&size) && imsg_.ParseFromCodedStream(&cis);
// }

bool Connection::unpack_msg_length(uint32_t& size) {
  std::istream is(&ibuf_);
  google::protobuf::io::IstreamInputStream iis(&is);
  google::protobuf::io::CodedInputStream cis(&iis);
  return cis.ReadLittleEndian32(&size);
}

// unpack_msg_body must be called after the success call of unpack_msg_length
bool Connection::unpack_msg_body() {
  std::istream is(&ibuf_);
  google::protobuf::io::IstreamInputStream iis(&is);
  google::protobuf::io::CodedInputStream cis(&iis);
  return imsg_.ParseFromCodedStream(&cis);
}

void Connection::save_remote() {
  if (!remote_record_valid_) {
    if (sending_) {
      remote_ip_ = endpoint_it_->endpoint().address().to_string();
      remote_port_ = endpoint_it_->endpoint().port();
      remote_record_valid_ = true;
    } else {
      try {
        remote_ip_ = socket_.remote_endpoint().address().to_string();
        remote_port_ = socket_.remote_endpoint().port();
        remote_record_valid_ = true;
      } catch (const std::exception& e) {
        remote_ip_ = "?:?:?:?";
        remote_port_ = 0;
        remote_record_valid_ = false;
      }
    }
  }
}

void Connection::save_local() {
  if (!local_record_valid_) {
    if (sending_) {
      if (sending_socket_) {
        local_ip_ = sending_socket_->local_endpoint().address().to_string();
        local_listening_port_ = node_.acceptor_.local_endpoint().port();
        local_record_valid_ = true;
      } else {
        local_ip_ = "?:?:?:?";
        local_listening_port_ = 0;
        local_record_valid_ = false;
      }
    } else {
      try {
        local_ip_ = socket_.local_endpoint().address().to_string();
        local_listening_port_ = node_.acceptor_.local_endpoint().port();
        local_record_valid_ = true;
      } catch (const std::exception& e) {
        local_ip_ = "?:?:?:?";
        local_listening_port_ = 0;
        local_record_valid_ = false;
      }
    }
  }
}

void Connection::set_origin(PeerMessage& out_msg) {
  std::ostringstream os;
  save_local();
  os << local_ip_ << ":" << local_listening_port_;
  omsg_.set_origin(os.str());
}

void Connection::error(const char* func_name, std::string err_msg) {
  save_remote();
  save_local();

  conn_log->error("{}:{} <--> [{}:{}] #{} error: {}", remote_ip_, remote_port_,
                  local_ip_, local_listening_port_, func_name, err_msg);
}

// should not be called in production
template <typename... VarArgs>
void Connection::debug_log(const char* fmt, VarArgs... var_args) {
  save_remote();

  std::string new_fmt{"{}:{} "};
  new_fmt += fmt;

  conn_log->debug(new_fmt.c_str(), remote_ip_, remote_port_, var_args...);
}

} /* end of namespace p2p_sgx */
