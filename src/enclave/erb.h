#ifndef __ENCLAVE_SRBP_H__
#define __ENCLAVE_SRBP_H__

#include <cstddef>
#include <memory>
#include <unordered_set>

#include "enclave.h"
#include "util.h"

namespace p2p_sgx {
namespace enclave {

class Core;

class SrbpMsg {
 public:
  SrbpMsg(rapidjson::Document &d) : d_(d) {}

  std::string type() { return d_["type"].GetString(); }
  uint64_t id() { return d_["id"].GetUint64(); }
  uint64_t round() { return d_["round"].GetUint64(); }
  uint64_t seq() { return d_["seq"].GetUint64(); }
  std::string m() { return d_["m"].GetString(); }

  void SetAck() {
    using namespace rapidjson;
    Document::AllocatorType &da = d_.GetAllocator();

    std::auto_ptr<char> prev_msg(p2p_sgx::util::NewBufferFromJson(d_));

    std::string base64 = base64_sha256(prev_msg.get());

    d_["type"].SetString("ACK", da);
    d_["m"].SetString(base64.c_str(), base64.size(), da);
  }

  void CopyToDoc(rapidjson::Document &goal) {
    goal.CopyFrom(d_, goal.GetAllocator());
  }

  rapidjson::Document &Document() { return d_; }

 private:
  rapidjson::Document &d_;
};

class SRBP {
 public:
  friend class ERNG;
  typedef rapidjson::Document *MsgDoc;
  typedef std::tr1::unordered_set<uint64_t, Hash> IdSet;
  enum StateType {
    kUninitialized,
    kRunning,
    kHalt,
    kDone  // kDone means round > t + 2
  };

  static uint64_t kRoundCycle;      // unit: seconds
  static uint64_t kRoundSendDelay;  // unit: seconds

  SRBP(Core &core, uint64_t init_id);
  SRBP(const SRBP &);
  ~SRBP();

  /*
   * Life cycle management
   */
  void NonInitiatorStart(uint64_t t);
  void InitiatorStart(uint64_t t, std::string &m);
  void Halt();
  void Done();
  bool IsDelivered();

  std::string GetM();

  int Handler(FullAddress source, SrbpMsg &msg);
  void TimerHandler();

  /*
   * Debug functions
   */
  void SetShortInitId(int short_init_id);

 private:
  struct InternalMsg {
    uint64_t id, seq, round;
    std::string type, m;
  };

  void reset();

  uint64_t get_init_seq();
  void inc_init_seq();

  void multicast_defer_echo(FullAddress &source, SrbpMsg &msg);
  void multicast_release();

  void unset_last_multicast();
  bool have_last_multicast();
  bool check_init_msg(FullAddress &source, SrbpMsg &msg);
  bool check_echo_msg(FullAddress &source, SrbpMsg &msg);
  bool check_ack_msg(FullAddress &source, SrbpMsg &msg);
  bool check_and_deliver();

  void deliver();
  void print_info();

  Core &core_;
  uint64_t init_id_;  // the initiator id
  int s_init_id_;     // short initiator id for test

  /*
   * Life cycle status (can be reset)
   */
  uint64_t init_time_;  // the time it is initialized

  /*
   * SRBP basic status
   */
  int n_;              // total number (including this one)
  int t_;              // total number of byzantine nodes
  uint64_t round_;     // current round
  std::string m_hat_;  // message to be delivered
  StateType state_;    // current state

  /*
   * SRBP extra status
   */
  bool is_delivered_;
  InternalMsg last_msg_, next_msg_;
  std::string last_msg_hash_;  // the hash of full bytes of last message
  std::string next_msg_str_;
  IdSet ack_;     // for last_multicast_
  IdSet s_echo_;  // collected ECHOs
};

} /* end of namespace enclave */
} /* end of namespace p2p_sgx */

#endif /*  __ENCLAVE_SRBP_H__ */
