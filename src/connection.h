#ifndef __CONNECTION_H__
#define __CONNECTION_H__

#include <array>
#include <iostream>
#include <memory>
#include <queue>
#include <unordered_set>

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <boost/asio.hpp>

#include "log.h"
#include "message.pb.h"

namespace p2p_sgx {

// forward declaration
class Node;

class Connection : public std::enable_shared_from_this<Connection> {
 public:
  const static std::size_t kLengthBytes; // sizeof(uint32_t)
  const static std::size_t kIOBufSize;

  static std::size_t kStartDelay;     // milliseconds
  static std::size_t kReconnectWait;  // milliseconds
  using boost_tcp = boost::asio::ip::tcp;
  Connection(const Connection&) = delete;
  Connection& operator=(const Connection&) = delete;

  Connection(boost_tcp::socket socket, Node& node);
  Connection(std::string& receiver, Node& node);

  // Connection(boost_tcp::socket socket, boost_tcp::resolver::iterator ep_it,
  // PeerMessage msg, Node& node);

  ~Connection();

  void Start();
  void Terminate();

  void Send(PeerMessage msg);
  void CloseSending();

 private:
  // void AsyncConnect();

  // for reading request and writing reply;
  void AsyncRead();
  void AsyncWrite();
  void AsyncReadLength();
  void AsyncReadBody(std::size_t size);

  // for sending request and receiving response;
  void AsyncSend(PeerMessage msg);
  void AsyncReceive();
  void AsyncReceiveBody(std::size_t size);
  void AsyncReceiveLength();

  bool pack_msg();  // omsg_ will be serialized to obuf_
  bool pack_msg(boost::asio::streambuf& obuf);
  bool unpack_msg();  // imsg_ will be parsed from ibuf_
  bool unpack_msg_length(
      uint32_t& size);     // imsg_length will be parsed from ibuf_
  bool unpack_msg_body();  // imsg_body will be parsed from ibuf_

  void set_origin(PeerMessage& out_msg);
  void save_remote();
  void save_local();
  void error(const char* func_name, std::string error_msg);

  void Dispatch();

  bool sending_{false};
  int reconnect_count_{10};

  template <typename... VarArgs>
  void debug_log(const char* fmt, VarArgs... var_args);

  // boost_tcp::endpoint ep_; // keep it for re-connect
  boost_tcp::resolver::iterator endpoint_it_;

  boost_tcp::socket socket_;
  boost::asio::streambuf ibuf_;
  boost::asio::streambuf obuf_;
  boost::asio::deadline_timer timer_;
  PeerMessage imsg_, omsg_;

  std::shared_ptr<boost_tcp::socket> sending_socket_;
  std::queue<PeerMessage> omsg_queue_;
  std::unordered_set<std::shared_ptr<boost::asio::streambuf>> obuf_set_;

  std::string remote_ip_;
  uint16_t remote_port_;
  bool remote_record_valid_{false};
  std::string local_ip_;
  uint16_t local_listening_port_;
  bool local_record_valid_{false};

  Node& node_;
};

using ConnectionPtr = std::shared_ptr<Connection>;

} /* end of namespace p2p_sgx */

#endif /* __CONNECTION_H__ */
