#ifndef __UTIL_H__
#define __UTIL_H__
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/writer.h>

namespace p2p_sgx {
namespace util {

#define CHANNEL_MIN_SIZE 512

char* NewBufferFromJson(const rapidjson::Document& d);

enum Base64OpType {
  kBase64Encode,
  kBase64Decode,
};

int b64_op(char* in, int in_len, char* out, int out_len, int op);

char* Base64Encode(const char* input, int length, bool with_new_line);
char* Base64Decode(char* input, int length, bool with_new_line);

}
}

/* Commmon macros for app and enclave */

#define EPH_SUCCESS 0
#define EPH_FAILURE 1

#endif
