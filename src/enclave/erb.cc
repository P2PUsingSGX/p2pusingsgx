#include "erb.h"
#include "core.h"

namespace p2p_sgx {
namespace enclave {

uint64_t SRBP::kRoundCycle;      // unit: seconds
uint64_t SRBP::kRoundSendDelay;  // unit: seconds

SRBP::SRBP(Core &core, uint64_t init_id)
    : core_(core), init_id_(init_id), n_(core.TotalNodes()), t_((n_ - 1) / 2) {
  reset();  // prefer to reset
}

/*
 * copy constructor
 */
SRBP::SRBP(const SRBP &s)
    : core_(s.core_), init_id_(s.init_id_), n_(s.n_), t_(s.t_) {
  reset();
  s_init_id_ = s.s_init_id_;
}

SRBP::~SRBP() {}

// for non-initiators
void SRBP::NonInitiatorStart(uint64_t t) {
  reset();
  init_time_ = t;
  state_ = kRunning;
}

// for initiator
void SRBP::InitiatorStart(uint64_t t, std::string &m) {
  using namespace rapidjson;

  NonInitiatorStart(t);

  m_hat_ = m;
  s_echo_.insert(init_id_);
  /*
   * setup next_multicast_ . it will be sent when TimeHandler is called
   */
  Document d;
  d.SetObject();
  Document::AllocatorType &da = d.GetAllocator();

  d.AddMember("type", "INIT", da);
  d.AddMember("id", init_id_, da);
  d.AddMember("seq", core_.GetPeerSeq(init_id_), da);
  d.AddMember("round", round_, da);
  d.AddMember("m", Value(m.c_str(), da).Move(), da);

  next_msg_.type = "INIT";
  next_msg_.id = init_id_;
  next_msg_.seq = core_.GetPeerSeq(init_id_);
  next_msg_.round = round_;
  next_msg_.m = m;

  StringBuffer d_buf;
  Writer<StringBuffer> writer(d_buf);
  d.Accept(writer);

  next_msg_str_ = d_buf.GetString();
  log_debug("[%llu] InitiatorStart %s", init_id_, next_msg_str_.c_str());
}

void SRBP::Halt() {
  if (state_ != kHalt) {
    log_critical("[%d] HALT", s_init_id_);
  }
  state_ = kHalt;
}

void SRBP::Done() {
  state_ = kDone;
  if (s_echo_.size() + t_ < n_) {
    m_hat_ = "";
    is_delivered_ = true;
    deliver();
    log_warn("[%d] SRBP Done", s_init_id_);
  }

  inc_init_seq();
  log_info("[%d] SRBP init_seq inc(%llu)", s_init_id_, get_init_seq());
}

void SRBP::TimerHandler() {
  if (state_ != kRunning) {
    return;  // stop TimerHandler
  }

  if (round_ > t_ + 2 && state_ != kDone) {
    Done();
    return;  // stop TimerHandler
  }
  // log_debug("[%d] #%s kRoundCycle %llu kRoundSendDelay %llu", s_init_id_,
  // __func__, kRoundCycle, kRoundSendDelay);

  uint64_t elapsed = core_.GetCurrentTime() - init_time_;

  if (elapsed % kRoundCycle == kRoundSendDelay) {
    if (have_last_multicast() && ack_.size() < t_) {
      Halt();
      return;  // XXX: newly added
    }
    multicast_release();
    // log_debug("[%d] #%s in RELEASE kRoundCycle %llu kRoundSendDelay %llu",
    // s_init_id_, __func__, kRoundCycle, kRoundSendDelay);
  }

  // log_debug(
  // "[%d] #%s elapsed = %llu mod = %llu kRoundCycle %llu kRoundSendDelay "
  // "%llu",
  // s_init_id_, __func__, elapsed, elapsed % kRoundCycle, kRoundCycle,
  // kRoundSendDelay);
  // enter next round
  uint64_t new_round = 1 + elapsed / kRoundCycle;
  if (new_round > round_) {
    log_info("================== [%d] Round %llu Start ==========", s_init_id_,
             new_round);
    round_ = new_round;
  }
}

int SRBP::Handler(FullAddress source, SrbpMsg &msg) {
  using namespace rapidjson;

  if (state_ != kRunning) {
    if (state_ == kUninitialized)
      log_error(source, "[%d] state = kUninitialized, need initialization",
                s_init_id_);
    else if (state_ == kHalt) {
      log_error(source, "[%d] state = kHalt, called by handler", s_init_id_);
    }
    return EPH_FAILURE;
  }

  if (round_ <= t_ + 2) {
    if (msg.type() == "INIT" && check_init_msg(source, msg)) {
      log_info(source, "--> [%d] INIT(id: %llu, seq: %llu, round: %llu, m: %s)",
               s_init_id_, msg.id(), msg.seq(), msg.round(),
               msg.m().substr(0, 10).c_str());
      multicast_defer_echo(source, msg);
      m_hat_ = msg.m();
      s_echo_.insert(init_id_);
      s_echo_.insert(core_.HostId());
      msg.SetAck();
      log_info(source, "<-- [%d] ACK(INIT)", s_init_id_);
    } else if (msg.type() == "ECHO" && check_echo_msg(source, msg)) {
      if (m_hat_.empty()) {
        m_hat_ = msg.m();
        s_echo_.insert(core_.HostId());
        multicast_defer_echo(source, msg);
      }
      if (s_echo_.find(fa_to_id(source)) == s_echo_.end()) {  // not in s_echo
        s_echo_.insert(fa_to_id(source));
        if (s_echo_.size() + t_ == n_) {
          is_delivered_ = true;
          deliver();
        }
      }
      log_info(source, "--> [%d] ECHO(%s) %u", s_init_id_,
               msg.m().substr(0, 10).c_str(), s_echo_.size());
      msg.SetAck();
      log_info(source, "<-- [%d] ACK(ECHO)", s_init_id_);
    } else if (msg.type() == "ACK" && check_ack_msg(source, msg)) {
      ack_.insert(fa_to_id(source));
      log_info(source, "--> [%d] ACK(%s) %u", s_init_id_,
               last_msg_hash_.substr(0, 10).c_str(), ack_.size());
      if (have_last_multicast() && ack_.size() >= t_) {
        unset_last_multicast();
      }
    } else if (msg.type() != "ACK" && msg.type() != "ECHO" &&
               msg.type() != "INIT") /* unknown message type  */ {
      log_error("xxx [%d] UNKNOWN message", s_init_id_);
      return EPH_FAILURE;
    }
  } else if (state_ != kDone) {
    Done();
    return EPH_FAILURE;
  }

  return EPH_SUCCESS;
}

bool SRBP::have_last_multicast() { return !last_msg_hash_.empty(); }
void SRBP::unset_last_multicast() { last_msg_hash_ = ""; }

bool SRBP::IsDelivered() { return is_delivered_; }

std::string SRBP::GetM() { return m_hat_; }

// private member functions
void SRBP::reset() {
  init_time_ = 0;
  round_ = 1;
  m_hat_ = "";
  state_ = kUninitialized;
  is_delivered_ = false;

  last_msg_ = next_msg_ = InternalMsg();
  last_msg_hash_ = "";
  next_msg_str_ = "";

  ack_.clear();
  s_echo_.clear();
}

uint64_t SRBP::get_init_seq() { return core_.GetPeerSeq(init_id_); }

void SRBP::inc_init_seq() { core_.IncPeerSeq(init_id_); }

bool SRBP::check_init_msg(FullAddress &source, SrbpMsg &msg) {
  if (msg.round() != round_) {
    log_error(source, "--> [%d] INIT msg.round(%llu) != round(%llu)",
              s_init_id_, msg.round(), round_);
    return false;
  }
  if (msg.seq() != get_init_seq()) {
    log_error(source, "--> [%d] INIT msg.seq(%llu) != seq(%llu)", s_init_id_,
              msg.seq(), get_init_seq());
    return false;
  }
  return true;
}

bool SRBP::check_echo_msg(FullAddress &source, SrbpMsg &msg) {
  if (msg.round() != round_) {
    log_error(source, "--> [%d] ECHO msg.round(%llu) != round(%llu)",
              s_init_id_, msg.round(), round_);
    return false;
  }
  if (msg.seq() != get_init_seq()) {
    log_error(source, "--> [%d] ECHO msg.seq(%llu) != seq(%llu)", s_init_id_,
              msg.seq(), get_init_seq());
    return false;
  }
  return true;
}

bool SRBP::check_ack_msg(FullAddress &source, SrbpMsg &msg) {
  if (!have_last_multicast()) {
    // log_warn(source, "--> [%d] ACK ignore: not expect any ACKs", s_init_id_);
    return false;
  }
  if (msg.round() != round_) {
    log_error(source, "--> [%d] ACK msg.round(%llu) != round(%llu)", s_init_id_,
              msg.round(), round_);
    return false;
  }
  if (msg.seq() != get_init_seq()) {
    log_error(source, "--> [%d] ACK msg.seq(%llu) != seq(%llu)", s_init_id_,
              msg.seq(), get_init_seq());
  }
  if (msg.m() != last_msg_hash_) {
    log_error(source, "--> [%d] ACK msg.m(%s) != last_hash(%s)", s_init_id_,
              msg.m().c_str(), last_msg_hash_.c_str());
    return false;
  }
  return true;
}

bool SRBP::check_and_deliver() {
  if (s_echo_.size() + t_ >= n_) {
    deliver();
    return true;
  }

  return false;
}

void SRBP::multicast_defer_echo(FullAddress &source, SrbpMsg &msg) {
  using namespace rapidjson;
  log_info(source, "||| [%d] ECHO(deferred)", s_init_id_);

  Document d;
  Document::AllocatorType &da = d.GetAllocator();
  d.SetObject();

  Value value;
  value.SetString("ECHO", 4, da);
  d.AddMember("type", value, da);
  d.AddMember("id", msg.id(), da);
  d.AddMember("seq", msg.seq(), da);
  d.AddMember("round", msg.round() + 1, da);
  value.SetString(msg.m().c_str(), msg.m().length(), da);
  d.AddMember("m", value, da);

  next_msg_.type = "ECHO";
  next_msg_.id = msg.id();
  next_msg_.seq = msg.seq();
  next_msg_.round = msg.round() + 1;
  next_msg_.m = msg.m();

  StringBuffer buf;
  Writer<StringBuffer> writer(buf);
  d.Accept(writer);

  next_msg_str_ = buf.GetString();
}

void SRBP::multicast_release() {
  log_info("================== [%d] Round %llu Release ==================",
           s_init_id_, round_);
  if (next_msg_str_ == "") {
    log_warn("<^> [%d] No message for release", s_init_id_);
    return;
  }
  // fix the bug where multicast_defer_echo is called before mulitcast_release
  // in same round
  if (next_msg_.round != round_) {
    log_warn("<^> [%d] Too hurry for next_msg.round(%llu)", s_init_id_,
             next_msg_.round);
    return;
  }
  log_info("<^> [%d] %s(id: %llu, seq: %llu, round: %llu, m: %s", s_init_id_,
           next_msg_.type.c_str(), next_msg_.id, next_msg_.seq, next_msg_.round,
           next_msg_.m.substr(0, 10).c_str());

  last_msg_ = next_msg_;

  core_.Multicast(next_msg_str_);
  last_msg_hash_ = base64_sha256((char *)next_msg_str_.c_str());

  // clear multicast statuss
  next_msg_str_ = "";
  ack_.clear();
}

void SRBP::print_info() {
  std::string state;
  if (state_ == kUninitialized)
    state = "uninitialized";
  else if (state_ == kRunning)
    state = "running";
  else if (state_ == kHalt)
    state = "halt";
  log_info("================ SRBP Round %llu report ================", round_);
  log_info(" n = %llu t = %llu state = %s", n_, t_, state.c_str());
  log_info(" m = %s", m_hat_.c_str());
  log_info(" S_echo ");
  for (IdSet::iterator it = s_echo_.begin(); it != s_echo_.end(); it++) {
    log_info("\t%llu", *it);
  }
  log_info("===================================================");
}

void SRBP::deliver() {
  log_critical("[%d] Deliver message %s", s_init_id_, m_hat_.c_str());
}

void SRBP::SetShortInitId(int short_init_id) { s_init_id_ = short_init_id; }

} /* end of namespace p2p_sgx */
} /* end of namespace enclave */
