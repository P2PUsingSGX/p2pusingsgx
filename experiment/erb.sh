#!/bin/bash

log_root=./log
time_path=`date +"%Y%m%d-%H%M%S"`
log_dir=$log_root/$time_path

run() {
  mkdir -p $log_dir
  echo $log_dir > current_log_path

  n=$1
  erb_n=$2
  round=$3
  delay=$4

  echo $n nodes running
  echo log path: $log_dir

  for i in `seq $n`
  do
    ./p2psgx -t$i/$n -r $round -d $delay -n $erb_n -m 1 -v 1> $log_dir/${i}_${n}.log 2>&1 &
  done
}

usage() {
  echo "Usage: $1 n erb_n round delay"
}


if [ -z $1 ] || [ -z $2 ] || [ -z $3 ] || [ -z $4 ]
then
  usage $0
  exit 1
fi

run $1 $2 $3 $4

