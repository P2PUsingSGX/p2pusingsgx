#ifndef __ENCLAVE_ERNG_H__
#define __ENCLAVE_ERNG_H__

#include <rapidjson/document.h>
#include <memory>
#include <set>
#include <string>
#include <unordered_set>
#include <unordered_map>

#include "enclave.h"
#include "util.h"

namespace p2p_sgx {
namespace enclave {

class ErngMsg {
 public:
  friend class Core;
  ErngMsg(rapidjson::Document &d) : d_(d) {}

  std::string type() { return d_["type"].GetString(); }
  uint64_t id() { return d_["id"].GetUint64(); }
  uint64_t round() { return d_["round"].GetUint64(); }
  uint64_t seq() { return d_["seq"].GetUint64(); }
  std::set<std::string> set_m() {
    std::set<std::string> ret;
    for (std::size_t i = 0; i < d_["m"].GetArray().Size(); i++) {
      ret.insert(d_["m"][i].GetString());
    }
    return ret;
  }

  void SetAck() {
    using namespace rapidjson;
    Document::AllocatorType &da = d_.GetAllocator();

    std::auto_ptr<char> prev_msg(p2p_sgx::util::NewBufferFromJson(d_));

    std::string base64 = base64_sha256(prev_msg.get());

    d_["type"].SetString("ACK", da);
    d_["m"].SetString(base64.c_str(), base64.size(), da);
  }

  void CopyToDoc(rapidjson::Document &goal) {
    goal.CopyFrom(d_, goal.GetAllocator());
  }

  rapidjson::Document &Document() { return d_; }

 private:
  rapidjson::Document &d_;
};

class Core;

class ERNG {
 public:
  friend class Core;
  typedef std::tr1::unordered_set<uint64_t, Hash> IdSet;
  typedef std::tr1::unordered_map<uint64_t, uint64_t, Hash> IdSeqMap;
  typedef std::set<std::string> MSet;
  enum StateType { kUninitialized, kRunning, kHalt, kAccepted };

  ERNG(Core &core, uint64_t id, uint64_t r1_max, uint64_t r2_max,
       uint64_t gamma);
  ~ERNG();

  void Start(uint64_t start_t);
  int Handler(FullAddress, ErngMsg &msg);
  void TimerHandler();

  bool IsAccepted();
  uint64_t GetReliableRand();

  const IdSet &GetChosen() { return s_chosen_; }

 private:
  struct InternalMsg {
    uint64_t id, seq, round;
    std::string type, m;
  };
  uint64_t get_rand(uint64_t upper_bound);
  void reset();
  void accept();
  void halt();
  void multicast_defer(std::string type, uint64_t id, uint64_t seq,
                       const char *m, uint64_t round);
  void multicast_defer_final();
  void multicast_release();
  std::string get_base64_uint64(uint64_t rand_m);
  void inc_all_seq();

  bool check_chosen_msg(FullAddress &source, ErngMsg &msg);
  bool check_final_msg(FullAddress &source, ErngMsg &msg);

  /* ERNG settings */
  uint64_t n_;  // TODO: initialize
  uint64_t t_;
  uint64_t id_;
  uint64_t seq_prime_;  // the backup of init's seq

  /* status */
  uint64_t rnd_;
  IdSet s_chosen_;
  IdSeqMap seq_backup_;
  MSet m_, s_final_;
  std::set<MSet> s_m_;
  uint64_t rand_r_;  // the output random number

  /* security parameters */
  int gamma_;  // TODO: initialize

  /* two random numbers */
  uint64_t r1_, r2_;  // TODO: initialize
  uint64_t r1_max_, r2_max_;

  std::string next_msg_str_;

  StateType state_;
  Core &core_;

  /* time */
  uint64_t init_time_;
};

} /* end of namespace enclave */
} /* end of namespace p2p_sgx */

#endif /*  __ENCLAVE_ERNG_H__ */
