.PHONY: all

src_dir := ./src

all:
	$(MAKE) -C $(src_dir) -j4
	@mv $(src_dir)/p2psgx $(src_dir)/enclave.signed.so experiment

clean:
	$(MAKE) -C $(src_dir) clean
	@rm -rf experiment/p2psgx experiment/enclave.signed.so
