#ifndef __USER_TYPE_H__
#define __USER_TYPE_H__

#include <sgx_ecp_types.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct FullAddress_ {
#ifdef __cplusplus
 public:
  inline bool operator==(const FullAddress_ &other) const {
    return ip == other.ip && port == other.port;
  }
#endif
  uint16_t port;
  uint8_t align[2];
  uint32_t ip;
} FullAddress;

typedef struct InternalConfig_ {
  int erb_round_time;
  int erb_round_delay_time;
  int erb_test_n;
  int erb_test_mode;
  int erng_r1_max;
  int erng_r2_max;
} InternalConfig;

#ifdef __cplusplus
}
#endif

#endif /* __USER_TYPE_H__ */
